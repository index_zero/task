#include "taskdetailcontrols.h"

TaskDetailComboBox::TaskDetailComboBox(QWidget *parent) :
    QComboBox(parent)
{
    setObjectName("taskdetailcombobox");
    setStyleSheet("#taskdetailcombobox {"
                  "padding: 2px 15px 2px 2px;"
                  "font-size: 14px;"
                  "border: 1px solid #ffffff;"
                  "margin: 2px;"
                  "}"
                  "#taskdetailcombobox QAbstractItemView {"
                  "padding: 0px;"
                  "background-color: #f5f5f5;"
                  "border: 1px solid #888888;"
                  "}"
                  "#taskdetailcombobox::drop-down {"
                  "subcontrol-origin: padding;"
                  "subcontrol-position: center right;"
                  "image: url(:/images/16x16/bullet_arrow_down.png) 1;"
                  "}"
                  "#taskdetailcombobox:hover, #taskdetailcombobox:focus {"
                  "background-color: #f5f5f5;"
                  "border: 1px solid #888888;"
                  "}");
    setSizePolicy(QSizePolicy::Preferred,
                  QSizePolicy::MinimumExpanding);
}

TaskDetailLineEdit::TaskDetailLineEdit(QWidget *parent) :
    QLineEdit(parent)
{
    setObjectName("taskdetaillineedit");
    setStyleSheet("#taskdetaillineedit {"
                  "border: 0px;"
                  "padding: 2px 0px 2px 2px;"
                  "font-size: 14px;"
                  "margin: 2px;"
                  "}"
                  "#taskdetaillineedit:hover, #taskdetaillineedit:focus {"
                  "background-color: #f5f5f5;"
                  "border-bottom: 1px solid #888888;"
                  "}");
    setSizePolicy(QSizePolicy::Preferred,
                  QSizePolicy::MinimumExpanding);
}

TaskDetailPlainTextEdit::TaskDetailPlainTextEdit(QWidget *parent) :
    QPlainTextEdit(parent)
{
    setObjectName("taskdetailplaintextedit");
    setStyleSheet("#taskdetailplaintextedit {"
                  "padding: 0px 2px 2px 0px;"
                  "font-size: 14px;"
                  "border: 0px;"
                  "margin: 2px;"
                  "}"
                  "#taskdetailplaintextedit:hover, #taskdetailplaintextedit:focus {"
                  "background-color: #f5f5f5;"
                  "border: 1px solid #888888;"
                  "}");
    setSizePolicy(QSizePolicy::Preferred,
                  QSizePolicy::MinimumExpanding);
}

TaskDetailDateTimeEdit::TaskDetailDateTimeEdit(QWidget *parent) :
    QDateTimeEdit(parent)
{
    setObjectName("taskdetaildatetime");
    setStyleSheet("#taskdetaildatetime {"
                  "padding: 4px 15px 4px 2px;"
                  "font-size: 14px;"
                  "border: 0px;"
                  "margin: 2px;"
                  "}"
                  "#taskdetaildatetime:hover, #taskdetaildatetime:focus {"
                  "background-color: #f5f5f5;"
                  "border: 1px solid #888888;"
                  "}"
                  "#taskdetaildatetime::up-button {"
                  "subcontrol-origin: border;"
                  "subcontrol-position: top right;"
                  "image: url(:/images/16x16/bullet_arrow_up.png) 1;"
                  "}"
                  "#taskdetaildatetime::down-button {"
                  "subcontrol-origin: border;"
                  "subcontrol-position: bottom right;"
                  "image: url(:/images/16x16/bullet_arrow_down.png) 1;"
                  "}");
    setSizePolicy(QSizePolicy::Preferred,
                  QSizePolicy::MinimumExpanding);
}

TaskDetailHeaderView::TaskDetailHeaderView(QWidget *parent)
    : QHeaderView(Qt::Horizontal, parent)
{
    setStretchLastSection(true);
    setDefaultSectionSize(150);
    setSectionResizeMode(QHeaderView::ResizeToContents);
    setObjectName("taskdetailheaderview");
    setStyleSheet("#taskdetailheaderview::section {"
                  "color: rgb(8, 71, 124);"
                  "border: 1px solid #888888;"
                  "font-weight: bold;"
                  "font-size: 12px;"
                  "background-color: #ffffff;"
                  "padding: 2px;"
                  "border: 0px;"
                  "border-bottom: 1px solid #888888;"
                  "border-right: 1px solid #888888;"
                  "height: 16px;"
                  "}"
                  "#taskdetailheaderview::section:last {"
                  "border: 0px;"
                  "border-bottom: 1px solid #888888;"
                  "}"
                  "#taskdetailheaderview {"
                  "background-color: #ffffff;"
                  "border-bottom: 1px solid #888888;"
                  "}");
}

TaskDetailGroupBox::TaskDetailGroupBox(QWidget *parent)
    : QGroupBox(parent)
{
    setCheckable(true);
    setChecked(false);
    setFocusPolicy(Qt::NoFocus);
    setObjectName("taskdetailgroupbox");
    setStyleSheet("#taskdetailgroupbox {"
                  "border: 0px;"
                  "font-size: 14px;"
                  "font-weight: bold;"
                  "background-color: #ffffff;"
                  "padding-top: 10px;"
                  "}"
                  "#taskdetailgroupbox::title {"
                  "color: rgb(8, 71, 124);"
                  "padding: 2px;"
                  "}"
                  "#taskdetailgroupbox::indicator:unchecked {"
                  "image: url(:/images/16x16/bullet_arrow_right.png);"\
                  "width: 16px;"
                  "height: 16px;"
                  "}"
                  "#taskdetailgroupbox::indicator {"
                  "image: url(:/images/16x16/bullet_arrow_down.png);"
                  "width: 16px;"
                  "height: 16px;"
                  "}");
}

TaskDetailGroupWidget::TaskDetailGroupWidget()
    : QVBoxLayout()
{
    m_widget = new QWidget;
    m_layout = new QVBoxLayout;

    m_layout->addWidget(m_widget);
    addWidget(m_widget);
}

void TaskDetailGroupWidget::setInnerLayout(QLayout *layout)
{
    m_widget->setLayout(layout);
}

void TaskDetailGroupWidget::setVisible(bool visible)
{
    m_widget->setVisible(visible);
}

TaskDetailLabel::TaskDetailLabel(QWidget *parent)
    : QLabel(parent)
{
    initializeStyleSheets();
}

TaskDetailLabel::TaskDetailLabel(const QString& text, QWidget *parent)
    : QLabel(text, parent)
{
    initializeStyleSheets();
}

void TaskDetailLabel::initializeStyleSheets()
{
    m_normalStyleSheet.append("font-size: 14px;"
//                              "font-weight: bold;"
                              "color: rgb(8, 71, 124);"
                              "padding: 4px 2px 0px 2px;"
                              "qproperty-alignment: AlignRight;"
                              "border-color: #888888;"
                              "border-style: solid;"
                              "border-right: 1px solid #888888;"
                              "margin: 0px;");

    m_modifiedStyleSheet.append("font-size: 14px;"
//                                "font-weight: bold;"
                                "color: rgb(255, 16, 16);"
                                "padding: 4px 2px 0px 2px;"
                                "qproperty-alignment: AlignRight;"
                                "border-color: #888888;"
                                "border-style: solid;"
                                "border-right: 1px solid #888888;"
                                "margin: 0px;");

    setStyleSheet(m_normalStyleSheet);

    setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    setAlignment(Qt::AlignVCenter);
}

void TaskDetailLabel::setModified(bool modified)
{
    m_modified = modified;

    if (m_modified) {
        setStyleSheet(m_modifiedStyleSheet);
    } else {
        setStyleSheet(m_normalStyleSheet);
    }

    repaint();

    emit labelModified(m_modified);
}
