#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QToolButton>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QSettings>

#include "taskheadline.h"
#include "tasklist.h"
#include "taskadd.h"
#include "desktopwidget.h"
#include "desktoptaskbar.h"
#include "taskloginscreen.h"
#include "taskloadingscreen.h"
#include "global.h"
#include "taskmainwidget.h"

class TaskMainWindow : public QMainWindow, public GlobalListener
{
    Q_OBJECT
public:
    enum TaskStates {
        TASK_STATE_FIRST = 0,

        TASK_STATE_INIT = TASK_STATE_FIRST,
        TASK_STATE_LOGIN,
        TASK_STATE_LOADING,
        TASK_STATE_RUNNING,
        TASK_STATE_CLOSE,

        TASK_STATE_LAST,
        TASK_STATE_COUNT = (TASK_STATE_LAST - TASK_STATE_FIRST)
    };

    explicit TaskMainWindow();

    // GlobalListener
    virtual void handleLogin(QXmlStreamReader *xml);
    virtual void handleUserData(QXmlStreamReader *xml);
    virtual void handleCounts(QXmlStreamReader *xml);
    virtual void handleUsers(QXmlStreamReader *xml);
    virtual void handleTaskData(QXmlStreamReader *xml);
    virtual void handleTasks(QXmlStreamReader *xml);
    virtual void handleLinks(QXmlStreamReader *xml);

    void setTaskState(const TaskStates& state);

    void closeEvent(QCloseEvent *event);
protected:
    DataUser *m_data;

    struct TaskStateData
    {
        int id;
        GlobalState *state;
    };

    TaskStateData m_states[TASK_STATE_COUNT];
    TaskStates m_state;
    QSettings m_settings;

    void writeSettings();
    void readSettings();

    void createActions();

signals:

public slots:
    void onPreferences();
    void onLogout();
    void onImportTasks();
    void onLogin(const QString& username, const QString& password);

private:
    TaskMainWidget *m_TaskMain;
    TaskLoginScreen *m_screenLogin;
    TaskLoadingScreen *m_screenLoading;

    DesktopTaskbar *m_taskBar;
    DesktopWidget *m_TaskDesktop;
    QWidget *m_mainUI;
    QStatusBar *m_statusBar;

    QMenuBar *m_mainMenu;
    QMenuBar *m_loginMenu;

    QStackedLayout *m_panel;
};

#endif // MAINWINDOW_H
