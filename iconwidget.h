#ifndef QICONWIDGET_H
#define QICONWIDGET_H

#include <QWidget>
#include <QIcon>

class IconWidget : public QWidget
{
    Q_OBJECT
public:
    explicit IconWidget(QWidget *parent = 0);

    explicit IconWidget(const QIcon& icon, QWidget *parent = 0);
    
    void setWidgetIcon(const QIcon& icon);

    QIcon getWidgetIcon() const;
protected:
    virtual void paintEvent(QPaintEvent *event);

private:
    QIcon m_Icon;

signals:
    
public slots:
    
};

#endif // QICONWIDGET_H
