#include "taskobjectproperties.h"

#include <QPalette>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFrame>

#include "taskusericon.h"

TaskObjectProperties::TaskObjectProperties(QWidget *child, QWidget *parent) :
    QLabel(parent),
    m_Object(child)
{
    QPalette p;
    QColor c = p.color(QPalette::Window);
    QString style, bg;

    QVBoxLayout *main = new QVBoxLayout;
    QHBoxLayout *titlebox = new QHBoxLayout;
    QFrame *sep = new QFrame;

    m_Collapse = new QToolButton;
    m_Title = new QLabel;

    sep->setFrameStyle(QFrame::HLine | QFrame::Sunken);

    bg.sprintf("background-color: %s;", c.name().toLocal8Bit().data());
    style.sprintf("QLabel {"
                  "border-radius: 10px;"
                  "border: 2px solid black;"
                  "background-color: %s; }",
                  c.name().toLocal8Bit().data());

    setStyleSheet(style);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    setMinimumSize(64, 64);

    m_Title->setStyleSheet("border: 0px; color: #000000;");
    m_Title->setText("<b>User Profile</b>");

    m_Collapse->setToolTip("Collapse");
    m_Collapse->setIcon(QIcon(":/images/small/collapse.png"));

    m_Collapse->setAutoRaise(true);
    m_Collapse->setIconSize(QSize(16, 16));
    m_Collapse->setToolButtonStyle(Qt::ToolButtonIconOnly);
    m_Collapse->setStyleSheet(bg);

    titlebox->addWidget(m_Title);
    titlebox->addWidget(m_Collapse);

    m_Collapsed = false;

    connect(m_Collapse, SIGNAL(clicked()), this, SLOT(collapseExpand()));

    setAllVisible(!m_Collapsed);

    m_Object->setStyleSheet(bg);

    main->addLayout(titlebox);
    main->addWidget(sep);
    main->addWidget(m_Object);
    main->setSizeConstraint(QLayout::SetMinimumSize);

    setLayout(main);
}

void TaskObjectProperties::collapseExpand()
{
    m_Collapsed = !m_Collapsed;

    if (m_Collapsed)
    {
        m_Collapse->setToolTip("Expand");
        m_Collapse->setIcon(QIcon(":/images/small/expand.png"));
    }
    else
    {
        m_Collapse->setToolTip("Collapse");
        m_Collapse->setIcon(QIcon(":/images/small/collapse.png"));
    }

    setAllVisible(!m_Collapsed);
}

void TaskObjectProperties::setAllVisible(bool v)
{
    m_Object->setVisible(v);
}

void TaskObjectProperties::setTitle(const QString& title)
{
    m_Title->setText(title);
}
