#ifndef TASKHEADLINE_H
#define TASKHEADLINE_H

#include <QLabel>
#include <QToolButton>

class TaskHeadline : public QLabel
{
    Q_OBJECT
public:
    explicit TaskHeadline(QWidget *parent = 0);

    void setHeadlineText(const QString& text);
    void setHeadlineIcon(const QIcon& icon);

signals:

public slots:

private:
    QLabel *m_Icon;
    QLabel *m_Headline;
    QToolButton *m_DismissButton;
};

#endif // TASKHEADLINE_H
