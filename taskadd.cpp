#include "taskadd.h"

#include <QFormLayout>
#include <QLabel>

TaskAdd::TaskAdd(QWidget *parent) :
    QLabel(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout;
    QFormLayout *form = new QFormLayout;
    QLabel *name = new QLabel;

    m_Advanced = new QGroupBox;

    m_TaskBody = new QTextEdit;
    m_User = new QComboBox;
    m_ElapsedTime = new QTimeEdit();
    m_ElapsedTime->setDisplayFormat("hh'h' mm'm'");


    QPalette p;
    QColor c = p.color(QPalette::Window);
    QString style;

    setObjectName("taskaddwidget");
    style.sprintf("#taskaddwidget {"
                  "border-radius: 10px;"
                  "border: 2px solid black;"
                  "padding-left: 10px;"
                  "padding-right: 10px;"
                  "margin: 10px;"
                  "background-color: %s; }",
                  c.name().toLocal8Bit().data());
    setStyleSheet(style);

    form->addRow("Assigned User:", m_User);
    form->addRow("Time Spent:", m_ElapsedTime);

    name->setText(tr("Task Details:"));

    m_AdvancedOptions = new QVBoxLayout;

    m_Advanced->setFlat(true);
    m_Advanced->setCheckable(true);
    m_Advanced->setTitle(tr("Advanced Options"));
    m_Advanced->setChecked(false);

    m_Advanced->setLayout(m_AdvancedOptions);
    m_AdvancedOptions->setSpacing(0);

    m_Milestone = new QCheckBox;
    m_Priority = new QComboBox;
    m_DueDate = new QDateEdit;

    m_Milestone->setText("Milestone");

    m_Priority->addItem("Minor");
    m_Priority->addItem("Normal");
    m_Priority->addItem("Urgent");
    m_Priority->addItem("Critical");

    m_DueDate->setMinimumDate(QDate::currentDate().addDays(-365));
    m_DueDate->setMaximumDate(QDate::currentDate().addDays(365));
    m_DueDate->setDisplayFormat("dddd, MMMM d, yyyy");
    m_DueDate->setCalendarPopup(true);

    m_AdvancedOptions->addWidget(m_DueDate);
    m_AdvancedOptions->addWidget(m_Priority);
    m_AdvancedOptions->addWidget(m_Milestone);

    m_DueDate->setVisible(false);
    m_Priority->setVisible(false);
    m_Milestone->setVisible(false);

    connect(m_Advanced, SIGNAL(toggled(bool)), this, SLOT(showAdvanced(bool)));

    vbox->addWidget(name);
    vbox->addWidget(m_TaskBody);
    vbox->addLayout(form);
    vbox->addWidget(m_Advanced);

    setLayout(vbox);
}

void TaskAdd::showAdvanced(bool show)
{
    int m = 0;

    if (show == true) { m = 5; }

    m_AdvancedOptions->setMargin(m);
    m_AdvancedOptions->setSpacing(m);

    m_DueDate->setVisible(show);
    m_Priority->setVisible(show);
    m_Milestone->setVisible(show);
}
