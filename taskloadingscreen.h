#ifndef TASKLOADINGSCREEN_H
#define TASKLOADINGSCREEN_H

#include <QLabel>
#include <QPushButton>
#include <QProgressBar>
#include "taskiconlabel.h"

#include "global.h"

class TaskLoadingScreen : public QLabel, public GlobalState
{
    Q_OBJECT
public:
    explicit TaskLoadingScreen(GlobalListener *listener, QWidget *parent = 0);
    
    void beginState();
    void endState();

    void setLabel(const QString& sz, const QIcon& icon);

    void loadUserData(const int id);

    void initLoadData(const int taskcnt, const int usercnt);

    void loadedUser();
    void loadedTask();

    void displayError(const QString& err);
private:
    void updateProgress();

    TaskIconLabel *m_label;
    QProgressBar *m_progressBar;
    QPushButton *m_cancelBtn;

    GlobalListener *m_listener;

    int m_taskCnt;
    int m_taskCur;

    int m_userCnt;
    int m_userCur;

    bool m_error;

signals:
    
public slots:
    void onCancel();
};

#endif // TASKLOADINGSCREEN_H
