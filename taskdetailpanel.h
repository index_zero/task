#ifndef TASKDETAILPANEL_H
#define TASKDETAILPANEL_H

#include <QFrame>
#include <QToolBar>
#include <QToolButton>
#include <QGroupBox>
#include <QLineEdit>
#include <QDateTimeEdit>
#include <QComboBox>
#include <QLabel>
#include <QScrollArea>
#include <QPlainTextEdit>
#include <QTableWidget>
#include <QVBoxLayout>

#include "taskdetailcontrols.h"

class TaskDetailPanel : public QFrame
{
    Q_OBJECT
public:
    explicit TaskDetailPanel(QWidget *parent = 0);

protected:
    void setupToolbar();

    void createControls();

private:
    QToolBar *m_toolBar;
    QScrollArea *m_formArea;

    // controls
    TaskDetailLineEdit *m_editTitle;
    TaskDetailPlainTextEdit *m_editDesc;
    TaskDetailComboBox *m_ownerBox;
    TaskDetailDateTimeEdit *m_modifiedTime;

    TaskDetailGroupBox *m_actionsGroup;
    TaskDetailGroupBox *m_extrasGroup;
    TaskDetailGroupWidget *m_actionsWidget;
    TaskDetailGroupWidget *m_extrasWidget;

    QTableWidget *m_eventTable;

    TaskDetailComboBox *m_actionBox;
    QLabel *m_formWidget;
    QLabel *m_infoMsg;

    // labels
    TaskDetailLabel *m_labelTitle;
    TaskDetailLabel *m_labelDesc;
    TaskDetailLabel *m_labelOwner;
    TaskDetailLabel *m_labelActivity;
    TaskDetailLabel *m_labelModified;

signals:
    
public slots:
    void taskChanged(const int& taskid);
    void resizeEvent(QResizeEvent *ev);

    void actionToggle(bool toggle);
    void extraToggle(bool toggle);
};

#endif // TASKDETAILPANEL_H
