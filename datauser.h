#ifndef DATAUSER_H
#define DATAUSER_H

#include <QString>
#include <QVariant>
#include <QMap>

#define USERDATA_ID_INVALID (-1)

//struct DataPreferences
//{
//    QString m_zipcode;
//    QString m_dateFormat;
//    QString m_timeFormat;
//    int m_weatherUpdateFreq;

//    DataPreferences& operator=(const DataPreferences& rhs)
//    {
//        if (this != &rhs)
//        {
//            m_zipcode = rhs.m_zipcode;
//            m_dateFormat = rhs.m_dateFormat;
//            m_timeFormat = rhs.m_timeFormat;
//            m_weatherUpdateFreq = rhs.m_weatherUpdateFreq;
//        }

//        return *this;
//    }
//};

class DataPreferences
{
public:
    DataPreferences(void) { }
    ~DataPreferences(void) { }

    DataPreferences& operator=(const DataPreferences& rhs)
    {
        if (this != &rhs)
        {
            m_preferences = rhs.m_preferences;
        }

        return *this;
    }

    QVariant value(const QString& key);
    bool setValue(const QString& key, const QVariant& v);

    int size() const;
protected:
    typedef QMap<QString, QVariant> DataPreferencesMap;

    DataPreferencesMap m_preferences;
};

class DataLink
{
public:
    explicit DataLink(const QString& url = QString(),
                      const QString& name = QString(),
                      const QDateTime& timestamp = QDateTime());
    explicit DataLink(const QString& url);

    virtual ~DataLink() { }

    void setName(const QString& name) { m_name = name; }
    void setTimestamp(const QDateTime& timestamp) { m_timestamp = timestamp; }
    void setUrl(const QString& url) { m_url = url; }

    QString getUrl() const { return m_url; }
    QString getName() const { return m_name; }
    QDateTime getTimestamp() const { return m_timestamp; }

    DataLink& operator=(const DataLink& rhs)
    {
        if (this != &rhs)
        {
            m_timestamp = rhs.m_timestamp;
            m_url = rhs.m_url;
            m_name = rhs.m_name;
        }

        return *this;
    }
protected:
    QDateTime m_timestamp;
    QString m_url;
    QString m_name;
};

class DataUser
{
public:
    explicit DataUser();
    explicit DataUser(const int id);
    virtual ~DataUser();

    int getId() const { return m_id; }
    QString getName() const { return m_name; }
    QString getUsername() const { return m_username; }
    QString getEmail() const { return m_email; }
    QString getCompany() const { return m_company; }


    void setName(const QString& str) { m_name = str; }
    void setUsername(const QString& str) { m_username = str; }
    void setEmail(const QString& str) { m_email = str; }
    void setCompany(const QString& str) { m_company = str; }

    void setPrefernces(const DataPreferences& pref) { m_preferences = pref; }
    DataPreferences getPreferences() const { return m_preferences; }

    DataUser& operator=(const DataUser& rhs);
private:
    int m_id;
    QString m_name;
    QString m_username;
    QString m_email;
    QString m_company;
    DataPreferences m_preferences;
};

#endif // DATAUSER_H
