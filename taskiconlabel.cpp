#include "taskiconlabel.h"

TaskIconLabel::TaskIconLabel(QWidget *parent) : QLabel(parent)
{
    m_labelIcon = new IconWidget;
    m_label = new QLabel;
    m_Layout = new QHBoxLayout;

    m_Layout->addWidget(m_labelIcon);
    m_Layout->addWidget(m_label);
    m_Layout->setContentsMargins(0, 0, 0, 0);
    m_Layout->setSpacing(3);

    m_label->setVisible(false);
    m_labelIcon->setVisible(false);

    setLayout(m_Layout);
}

TaskIconLabel::TaskIconLabel(
    const QString& text,
    const QIcon& icon,
    QWidget *parent) : QLabel(parent)
{
    m_labelIcon = new IconWidget(icon);
    m_labelIcon->setFixedSize(icon.actualSize(QSize(1000, 1000), QIcon::Normal, QIcon::On));
    m_label = new QLabel;
    m_Layout = new QHBoxLayout;

    setLabel(text, icon);

    m_Layout->addWidget(m_labelIcon);
    m_Layout->addWidget(m_label);
    m_Layout->setContentsMargins(0, 0, 0, 0);
    m_Layout->setSpacing(3);

    setLayout(m_Layout);
}


void TaskIconLabel::setLabel(
    const QString& text,
    const QIcon& icon)
{
    m_label->setVisible(true);
    m_labelIcon->setVisible(true);

    m_labelIcon->setWidgetIcon(icon);
    m_labelIcon->setFixedSize(icon.actualSize(QSize(1000, 1000), QIcon::Normal, QIcon::On));
    m_label->setText(text);
}

void TaskIconLabel::resetLabel()
{
    m_label->setVisible(false);
    m_labelIcon->setVisible(false);
}
