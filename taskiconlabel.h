#ifndef TASKICONLABEL_H
#define TASKICONLABEL_H

#include <QLabel>
#include <QToolButton>
#include <QHBoxLayout>
#include <QIcon>
#include "iconwidget.h"

class TaskIconLabel : public QLabel
{
    Q_OBJECT
public:
    explicit TaskIconLabel(
        const QString& text,
        const QIcon& icon,
        QWidget *parent = 0);

    explicit TaskIconLabel(QWidget *parent = 0);

    void resetLabel();

    void setLabel(
        const QString& text,
        const QIcon& icon);
protected:
    IconWidget *m_labelIcon;
    QLabel *m_label;
    QHBoxLayout *m_Layout;

signals:

public slots:

};

#endif // TASKICONLABEL_H
