#include "taskloginscreen.h"

#include <QVBoxLayout>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QFormLayout>

#include <QThread>
#include <QMessageBox>
#include "global.h"
#include "taskglobal.h"

TaskLoginScreen::TaskLoginScreen(GlobalListener *listener, QWidget *parent) :
    QLabel(parent),
    m_listener(listener)
{
    QSpacerItem *top = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QSpacerItem *bottom = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QSpacerItem *left = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QSpacerItem *right = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QHBoxLayout *gridh = new QHBoxLayout;
    QVBoxLayout *gridv = new QVBoxLayout;
    QLabel *emptyLabel = new QLabel;

    QLabel *bg = new QLabel;
    m_directions = new QLabel("Enter your username and password in the boxes below to login.");
    m_directions->setWordWrap(true);
    m_labelUsername = new QLabel("Username");
    m_labelPassword = new QLabel("Password");
    m_rememberMe = new QCheckBox("Remember login credentials");

    m_labelIcon = new IconWidget(QIcon(":/images/group_key.png"));
    m_labelIcon->setFixedSize(32, 32);

//    m_labelIcon->setObjectName("labelIcon");
//    m_labelIcon->setStyleSheet("#labelIcon { background-color: rgba(0, 0, 0, 0%); }");
//    m_labelIcon->setAutoRaise(false);
//    m_labelIcon->setIconSize(QSize(32, 32));
//    m_labelIcon->setToolButtonStyle(Qt::ToolButtonIconOnly);
//    m_labelIcon->setWidgetIcon();
//    m_labelIcon->setAttribute(Qt::WA_TranslucentBackground);
//    m_labelIcon->setBackgroundRole(QPalette::Button);

    m_labelInfo = new TaskIconLabel();

    m_username = new QLineEdit;
    m_password = new QLineEdit;

    m_username->setFocus(Qt::ActiveWindowFocusReason);

    m_password->setEchoMode(QLineEdit::Password);

    m_loginButton = new QPushButton("Login");
    m_loginButton->setDefault(true);
    m_loginButton->setAutoDefault(true);

    m_quitButton = new QPushButton("Quit");
    //m_quitButton->addAction(TaskGlobal::getAction("Quit"));

    QHBoxLayout *hbox = new QHBoxLayout;
    QGridLayout *box = new QGridLayout;

    box->addWidget(m_labelIcon, 0, 0);
    box->addWidget(m_directions, 0, 1);
    box->addWidget(m_labelUsername, 1, 0);
    box->addWidget(m_username, 1, 1);
    box->addWidget(m_labelPassword, 2, 0);
    box->addWidget(m_password, 2, 1);
    box->addWidget(m_rememberMe, 3, 1);
    box->addWidget(m_labelInfo, 4, 1);

    hbox->addWidget(emptyLabel);
    hbox->addWidget(m_loginButton);
    hbox->addWidget(m_quitButton);

    box->addLayout(hbox, 5, 1);

    QPalette p;
    QColor c = p.color(QPalette::Window);
    QString style;

    bg->setObjectName("taskloginwidgetbg");
    style.sprintf("#taskloginwidgetbg {"
                  "border-radius: 10px;"
                  "border: 2px solid black;"
                  "padding-left: 10px;"
                  "padding-right: 10px;"
                  "margin: 10px;"
                  "background-color: %s; }",
                  c.name().toLocal8Bit().data());
    bg->setStyleSheet(style);
    bg->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    bg->setMinimumHeight(250);
    bg->setMinimumWidth(400);

    setObjectName("backgroudgradient");
    setStyleSheet(
        "#backgroudgradient { "
        "margin: 0px;"
        "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1,"
            "stop:0 #404040,stop:1 #000);"
        "color: #cfcfcf; }");
    //left->setMinimumWidth(100);
    setMinimumWidth(100);

    connect(m_loginButton, SIGNAL(clicked()), this, SLOT(loginClicked()));

    connect(m_quitButton, SIGNAL(clicked()), TaskGlobal::getApp(), SLOT(quit()));

    bg->setLayout(box);

    gridh->addItem(left);
    gridh->addWidget(bg, 0);
    gridh->addItem(right);

    gridv->addItem(top);
    gridv->addLayout(gridh, 0);
    gridv->addItem(bottom);

    setLayout(gridv);
}

void TaskLoginScreen::setButtonEnabled(bool enabled)
{
    m_loginButton->setEnabled(enabled);
}

void TaskLoginScreen::loginClicked()
{
    m_loginButton->setAutoDefault(false);
    m_loginButton->setDefault(false);
    m_loginButton->setEnabled(false);

    repaint();  // force repaint
    QThread::msleep(200);   // feedback of button press

    emit login(m_username->text().toHtmlEscaped(),
               m_password->text());
}

void TaskLoginScreen::showEvent(QShowEvent *event)
{
    QLabel::showEvent(event);

    m_username->setFocus();
}

void TaskLoginScreen::keyPressEvent(QKeyEvent *ev)
{
    QLabel::keyPressEvent(ev);

    if (ev->key() == Qt::Key_Return) {
        m_loginButton->click();
    }
}

void TaskLoginScreen::beginState()
{
    m_username->setText("");
    m_password->setText("");

    m_rememberMe->setChecked(false);

    m_loginButton->setDefault(true);
    m_loginButton->setAutoDefault(true);
    m_loginButton->setEnabled(true);

    m_labelInfo->resetLabel();

    m_username->setFocus();
}

void TaskLoginScreen::endState()
{

}

void TaskLoginScreen::mousePressEvent(QMouseEvent *ev)
{
    setFocus();

    Q_UNUSED(ev);
}
