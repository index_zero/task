#ifndef TASKLISTITEM_H
#define TASKLISTITEM_H

#include <QtGui/QColor>
#include <QGraphicsItem>
#include "task.h"

#define SIZE_BORDER     3
#define SIZE_SPACER     4
#define SIZE_BUTTONS    70
#define SIZE_COLOR      15
#define SIZE_HANDLE     5

#define SIZE_TITLE  22
#define SIZE_OWNER  14

class TaskListItem : public QGraphicsItem
{
public:
    TaskListItem(const int& taskid);

    QRectF boundingRect() const;
    QPainterPath shape() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);

    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    void setHighlight(const bool& highlight) { m_keepHighlight = highlight; }
    void setItemWidth(const int& width);

    int getId() const { return m_taskid; }
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    // draws the task or group color
    void drawTaskColor(QPainter *painter, bool highlight);
    void drawSeparator(QPainter *painter);

    void drawTitle(QPainter *painter);
    void drawOwner(QPainter *painter);
    void drawDescription(QPainter *painter);

    void drawIcons(QPainter *painter);

    void drawTime(QPainter *painter);

    void drawHighlight(QPainter *painter);

private:
    int m_itemWidth;

    QRectF m_boundingRect;
    QPixmap m_pixmap;
    bool m_highlight;
    bool m_keepHighlight;

    int m_taskid;
    Task *m_taskPointer;
};

#endif // TASKLISTITEM_H
