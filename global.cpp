#include "global.h"
#include "taskglobal.h"

#include <QCryptographicHash>
#include <QMessageBox>
#include <ctype.h>
#include <QDebug>

QByteArray GlobalNetworkObject::encode(const QString& data)
{
    QByteArray ret;

    for (int i = 0; i < data.size(); i++) {
        char c = data.at(i).toLatin1();

        if (isalnum(c) || (c == '.') || (c == '_') || (c == '-') || (c == '~')) {
            ret.append(c);
        } else {
            QString sz;
            sz.sprintf("%%%02hhx", c);
            ret.append(sz);
        }
    }

    return ret;
}

GlobalNetworkObject::GlobalNetworkObject(QObject * parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager();
    m_insideQuery = false;
    m_currentReply = NULL;
    m_lastFunction = qNONE;

    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished(QNetworkReply*)));
}

GlobalNetworkObject::~GlobalNetworkObject()
{
    if (m_currentReply != NULL) {
        m_currentReply->deleteLater();
        m_currentReply = NULL;
    }

    delete m_manager;
}

QNetworkAccessManager* GlobalNetworkObject::getNetworkManager()
{
    return m_manager;
}

void GlobalNetworkObject::finished(QNetworkReply *reply)
{
//    reply->deleteLater();
    if (reply == m_currentReply) {
        m_insideQuery = false;
    }
}

void GlobalNetworkObject::readReady()
{
    int rc = m_currentReply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    // Success 2xx
    if ((rc >= 200) && (rc < 300) && (m_insideQuery == true)) {
        QByteArray data = m_currentReply->readAll();

        m_xml.addData(data);
        m_replyData.append(data);
    }
}

void GlobalNetworkObject::readFinished()
{
    m_insideQuery = false;

//    QMessageBox b;
//    b.setText(m_replyData);
//    b.exec();

    qDebug() << "query End() <<<";

    switch (m_lastFunction)
    {
    case qLOGIN:
        m_listener->handleLogin(&m_xml);
        break;
    case qUSERDATA:
        m_listener->handleUserData(&m_xml);
        break;
    case qWEATHER:
        m_listener->handleWeatherData(&m_xml);
        break;
    case qCNT:
        m_listener->handleCounts(&m_xml);
        break;
    case qUSERS:
        m_listener->handleUsers(&m_xml);
        break;
    case qTASKDATA:
        m_listener->handleTaskData(&m_xml);
        break;
    case qTASKS:
        m_listener->handleTasks(&m_xml);
        break;
    case qLINKS:
        m_listener->handleLinks(&m_xml);
        break;
    default: break;
    }
}

void GlobalNetworkObject::metaDataChanged()
{
    QUrl redirect = m_currentReply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();

    if (redirect.isValid()) {
        getUrl(redirect);
    }
}

void GlobalNetworkObject::error(QNetworkReply::NetworkError err)
{
//    m_currentReply->disconnect(this);
    m_currentReply->deleteLater();

    if (m_insideQuery == true) {
        m_insideQuery = false;
    }

    QString sz;

    sz.sprintf("Network Error %d: ", err);
    sz.append(m_currentReply->errorString());

    m_currentReply = NULL;

    QMessageBox box;
    box.setIcon(QMessageBox::Critical);
    box.setStandardButtons(QMessageBox::Ok);
    box.setText(sz);
    box.exec();
}

void GlobalNetworkObject::query(GlobalListener *listener, QByteArray q, const QueryFunction& f)
{
    m_mutex.lock();

    if (m_insideQuery == false) {
        qDebug() << "query Begin(" << f << ") >>>";

        m_insideQuery = true;
        m_listener = listener;

        m_lastFunction = f;
        m_postData.clear();
        m_postData.append(q);

        m_xml.clear();
        m_replyData.clear();

//        QMessageBox msgBox;
//        msgBox.setText(m_postData);
//        msgBox.exec();

        getUrl(QUrl("http://www.theindexzero.com/tasks/task.php"));
    } else {
        qDebug() << "Rejecting query while another is already in process";
    }

    m_mutex.unlock();
}

void GlobalNetworkObject::query(GlobalListener *listener, QByteArray q, const QString& url, const QueryFunction& f)
{
    m_mutex.lock();

    if (m_insideQuery == false) {
        qDebug() << "query Begin(" << f << ") >>>";

        m_insideQuery = true;
        m_listener = listener;

        m_lastFunction = f;
        m_postData.clear();
        m_postData.append(q);

        m_xml.clear();
        m_replyData.clear();

//        QMessageBox msgBox;
//        msgBox.setText(m_postData);
//        msgBox.exec();

        getUrl(QUrl(url));
    } else {
        qDebug() << "Rejecting query while another is already in process";
    }

    m_mutex.unlock();
}

void GlobalNetworkObject::getUrl(const QUrl& url)
{
    QNetworkRequest request(url);

    if (m_currentReply)
    {
//        m_currentReply->disconnect(this);
        m_currentReply->deleteLater();
    }

    request.setHeader(QNetworkRequest::ContentTypeHeader,
                      QVariant("application/x-www-form-urlencoded"));

    m_currentReply = TaskGlobal::getNetworkManager()->post(request, m_postData);

    connect(m_currentReply, SIGNAL(readyRead()), this, SLOT(readReady()));
    connect(m_currentReply, SIGNAL(finished()), this, SLOT(readFinished()));
    connect(m_currentReply, SIGNAL(metaDataChanged()), this, SLOT(metaDataChanged()));
    connect(m_currentReply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
}


