#ifndef TASKOBJECTPROPERTIES_H
#define TASKOBJECTPROPERTIES_H

#include <QLabel>
#include <QToolButton>
#include <QVBoxLayout>

class TaskObjectProperties : public QLabel
{
    Q_OBJECT
public:
    explicit TaskObjectProperties(QWidget *child, QWidget *parent = 0);

    void setTitle(const QString& title);
signals:

public slots:
    void collapseExpand();

private:
    QLabel *m_Title;
    QToolButton *m_Collapse;
    QWidget *m_Object;

    void setAllVisible(bool v);
    bool m_Collapsed;
};

#endif // TASKOBJECTPROPERTIES_H
