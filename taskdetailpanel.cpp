#include "taskdetailpanel.h"

#include <QVBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QHeaderView>

#include "taskglobal.h"

TaskDetailPanel::TaskDetailPanel(QWidget *parent) :
    QFrame(parent)
{
    QVBoxLayout *mainBox = new QVBoxLayout;

    m_toolBar = new QToolBar;
    m_formArea = new QScrollArea;

    createControls();

    mainBox->addWidget(m_toolBar);
    mainBox->addWidget(m_formArea);
    mainBox->setContentsMargins(0, 0, 0, 0);
    mainBox->setSpacing(0);

    m_formArea->setObjectName("formArea");

    m_formArea->setStyleSheet("#formArea {"
                              "background-color: #ffffff;"
                              "}");

    QFormLayout *formLayout = new QFormLayout;
    formLayout->setRowWrapPolicy(QFormLayout::DontWrapRows);
    formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
    formLayout->setFormAlignment(Qt::AlignHCenter | Qt::AlignTop);
    formLayout->setLabelAlignment(Qt::AlignRight | Qt::AlignTop);
    formLayout->setSpacing(0);

    m_infoMsg = new QLabel;
    m_infoMsg->setSizePolicy(QSizePolicy::Preferred,
                             QSizePolicy::MinimumExpanding);

    formLayout->addRow(m_infoMsg);

//    QVBoxLayout *descLayout = new QVBoxLayout;
    m_labelDesc->setWordWrap(true);


    formLayout->addRow(m_labelTitle, m_editTitle);
    formLayout->addRow(m_labelDesc, m_editDesc);
    formLayout->addRow(m_labelOwner, m_ownerBox);
    formLayout->addRow(m_labelModified, m_modifiedTime);
    formLayout->addRow(m_labelActivity, m_eventTable);

    formLayout->addRow(m_extrasGroup);
    formLayout->addRow(m_actionsGroup);

    // default the user list
    TaskGlobal::UserList ul = TaskGlobal::getUsers();
    for (int i = 0; i < ul.size(); i++) {
        m_ownerBox->addItem(ul.at(i).getName());
    }

    formLayout->setSizeConstraint(QLayout::SetFixedSize);

    m_formWidget = new QLabel;
    m_formWidget->setScaledContents(true);
    m_formWidget->setLayout(formLayout);
//    m_formWidget->updateGeometry();

//    m_formArea->setLayout(formLayout);
    m_formArea->setWidget(m_formWidget);
//    m_formArea->setViewport(m_formWidget);
    m_formArea->setWidgetResizable(false);
    m_formArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_formArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    m_formWidget->setAutoFillBackground(false);

    connect(m_actionsGroup, SIGNAL(toggled(bool)), this, SLOT(actionToggle(bool)));
    connect(m_extrasGroup, SIGNAL(toggled(bool)), this, SLOT(extraToggle(bool)));

    m_actionsGroup->setLayout(m_actionsWidget);

    setupToolbar();

    setLayout(mainBox);
}

void TaskDetailPanel::setupToolbar()
{
    QToolButton *b = new QToolButton;

    b->setAutoRaise(true);
    b->setIconSize(QSize(32, 32));
    b->setText("Move to Trash");
    b->setToolButtonStyle(Qt::ToolButtonIconOnly);
    b->setIcon(QIcon(":/images/bin.png"));
    b->setFixedSize(48, 48);

    m_toolBar->addWidget(b);
}

void TaskDetailPanel::taskChanged(const int& taskid)
{
    int i;

    if (taskid != INVALID_TASK) {
//        qDebug() << "slot taskChanged(" << taskid << ")";
        Task *t = TaskGlobal::getTask(taskid);

        m_editTitle->setText(t->getTitle());
        m_editDesc->setPlainText(t->getDescription());

        // update and select owner
        TaskGlobal::UserList ul = TaskGlobal::getUsers();
        //t->getOwner() = ;

        m_ownerBox->clear();
        for (i = 0; i < ul.size(); i++) {
            m_ownerBox->addItem(ul.at(i).getName());
            if (ul.at(i).getId() == t->getOwner()) {
                m_ownerBox->setCurrentIndex(i);
            }
        }

        QString style;
        style.sprintf("#formArea {"
                      "background-color: #ffffff;"
                      "border-top: 5px solid;"
                      "border-top-color: rgb(%d, %d, %d);"
                      "}",
                      t->getColor().red(),
                      t->getColor().green(),
                      t->getColor().blue());
        m_formArea->setStyleSheet(style);


        m_modifiedTime->setDateTime(t->getLastModified());

        m_eventTable->clear();

        Task::TaskEventList el = t->getEvents();
        m_eventTable->setRowCount(t->getNumEvents());

        // set table
        QStringList sz;
        sz.append("Type");
        sz.append("User");
        sz.append("Time");
        m_eventTable->setHorizontalHeaderLabels(sz);
        m_eventTable->verticalHeader()->hide();
        for (i = 0; i < el.size(); i++) {
            QTableWidgetItem *itype = new QTableWidgetItem(el.at(i).toTypeString(el.at(i).getType()));
            QTableWidgetItem *iowner = new QTableWidgetItem(TaskGlobal::getUser(el.at(i).getUser()).getName());
            QTableWidgetItem *itime = new QTableWidgetItem(el.at(i).getTime().toString("yyyy-MM-dd hh:mm:ss"));

            m_eventTable->setItem(i, 0, itype);
            m_eventTable->setItem(i, 1, iowner);
            m_eventTable->setItem(i, 2, itime);
        }
    }
}

void TaskDetailPanel::createControls()
{
    m_editTitle = new TaskDetailLineEdit;

    m_editDesc = new TaskDetailPlainTextEdit;

    m_ownerBox = new TaskDetailComboBox;

    m_modifiedTime = new TaskDetailDateTimeEdit;


    m_labelTitle = new TaskDetailLabel("<b>Task Title</b>");
    m_labelDesc = new TaskDetailLabel("<b>Description</b><br />This field should contain plaintext only.");
    m_labelOwner = new TaskDetailLabel("<b>Owner</b>");
    m_labelActivity = new TaskDetailLabel("<b>Activity</b>");
    m_labelModified = new TaskDetailLabel("<b>Last Modified</b>");


    QString fmt;
    DataPreferences perf = TaskGlobal::getUserData().getPreferences();

    fmt.append(perf.value("dateformat").toString());
    fmt.append(" ");
    fmt.append(perf.value("timeformat").toString());

    m_modifiedTime->setReadOnly(true);
    m_modifiedTime->setDisplayFormat(fmt);
    m_modifiedTime->setDateTime(QDateTime::currentDateTime());

    QStringList sz;
    sz.append("Type");
    sz.append("User");
    sz.append("Time");
    TaskDetailHeaderView *header = new TaskDetailHeaderView;

    m_eventTable = new QTableWidget;
    m_eventTable->setColumnCount(3);
    m_eventTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    m_eventTable->setSelectionMode(QAbstractItemView::SingleSelection);
    m_eventTable->setHorizontalHeader(header);
    m_eventTable->setHorizontalHeaderLabels(sz);
    m_eventTable->setObjectName("taskdetailtablewidget");
    m_eventTable->setStyleSheet("#taskdetailtablewidget {"
                                "gridline-color: #ffffff;"
                                "border: 1px solid #ffffff;"
                                "background-color: #ffffff;"
                                "margin: 2px;"
                                "font-size: 14px;"
                                "selection-background-color: rgb(8, 71, 124);"
                                "selection-color: white;"
                                "}"
                                "#taskdetailtablewidget:hover {"
                                "background-color: #f5f5f5;"
                                "gridline-color: #f5f5f5;"
                                "border: 1px solid #888888;"
                                "}");


    m_actionsGroup = new TaskDetailGroupBox;
//    m_actionsGroup->setFlat(true);
    m_actionsGroup->setTitle("Perform Action");

    m_extrasGroup = new TaskDetailGroupBox;
    m_extrasGroup->setTitle("Extra Settings");

    m_actionBox = new TaskDetailComboBox;
    m_actionBox->addItem("Start working");
    m_actionBox->addItem("Stop working");
    m_actionBox->addItem("Schedule");
    m_actionBox->addItem("Finish");

    m_actionsWidget = new TaskDetailGroupWidget;
    m_extrasWidget =  new TaskDetailGroupWidget;

    QVBoxLayout *b = new QVBoxLayout;
    b->setContentsMargins(5, 5, 5, 0);
    b->addWidget(m_actionBox);
    b->setSizeConstraint(QLayout::SetMinimumSize);

    TaskDetailPlainTextEdit *p = new TaskDetailPlainTextEdit;
    b->addWidget(p);

    m_actionsWidget->setInnerLayout(b);
    m_actionsWidget->setVisible(false);
    m_extrasWidget->setVisible(false);
}

void TaskDetailPanel::resizeEvent(QResizeEvent *ev)
{
    int offset = 16;
    if (m_formArea->verticalScrollBar()->isVisible()) {
        offset += m_formArea->verticalScrollBar()->width();
    }

    m_infoMsg->setMinimumWidth(ev->size().width() - offset);
}

void TaskDetailPanel::actionToggle(bool toggle)
{
    int offset = 16;
    if (toggle) {
        offset += m_formArea->verticalScrollBar()->width();
    }

    m_infoMsg->setMinimumWidth(width() - offset);
    m_actionsWidget->setVisible(toggle);
}

void TaskDetailPanel::extraToggle(bool toggle)
{
    int offset = 16;
    if (toggle) {
        offset += m_formArea->verticalScrollBar()->width();
    }

    m_infoMsg->setMinimumWidth(width() - offset);
    m_extrasWidget->setVisible(toggle);
}

