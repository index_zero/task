#include "datauser.h"
#include <QDateTime>

DataUser::DataUser() : m_id(USERDATA_ID_INVALID)
{
}

DataUser::DataUser(const int id) : m_id(id)
{
}

DataUser::~DataUser()
{
}

DataUser& DataUser::operator=(const DataUser& rhs)
{
    if (this != &rhs)
    {
        m_id = rhs.m_id;
        m_name = rhs.m_name;
        m_username = rhs.m_username;
        m_email = rhs.m_email;
        m_company = rhs.m_company;
        m_preferences = rhs.m_preferences;
    }

    return *this;
}

QVariant DataPreferences::value(const QString& key)
{
    QVariant v;
    DataPreferencesMap::Iterator it = m_preferences.find(key);

    if (it != m_preferences.end()) {
        v = it.value();
    }

    return v;
}

bool DataPreferences::setValue(const QString& key, const QVariant& v)
{
    bool added = false;

    DataPreferencesMap::Iterator it = m_preferences.find(key);

    if (it == m_preferences.end()) {
        added = true;
    }

    m_preferences.insert(key, v);

    return added;
}

int DataPreferences::size() const
{
    return m_preferences.size();
}

DataLink::DataLink(const QString& url,
                   const QString& name,
                   const QDateTime& timestamp) :
    m_url(url),
    m_name(name),
    m_timestamp(timestamp)
{

}

DataLink::DataLink(const QString& url) : m_url(url)
{
    m_name = QString();
    m_timestamp = QDateTime::currentDateTime();
}
