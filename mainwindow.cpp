#include "mainwindow.h"
#include <QHBoxLayout>
#include <QMenuBar>
#include <QToolBar>
#include <QPushButton>
#include <QToolButton>
#include <QGridLayout>
#include <QListView>
#include <QLabel>
#include <QSplitter>
#include <QSplitterHandle>
#include <QDateTimeEdit>
#include <QLineEdit>
#include <QMessageBox>
#include <QTabWidget>
#include <QDebug>

#include "taskloginscreen.h"
#include "desktoptaskbar.h"
#include "taskusericon.h"
#include "taskglobal.h"

static QColor colorFromHexString(const QString& str)
{
    QColor ret;
    bool ok;

    int r = str.mid(0, 2).toInt(&ok, 16);
    int g = str.mid(2, 2).toInt(&ok, 16);
    int b = str.mid(4, 2).toInt(&ok, 16);

    ret.setRed(r);
    ret.setGreen(g);
    ret.setBlue(b);

    return ret;
}

TaskMainWindow::TaskMainWindow()
{
    QVBoxLayout *dbox = new QVBoxLayout;
    QWidget *panelWidget = new QWidget;

    createActions();

    m_mainUI = new QWidget;
    m_taskBar = new DesktopTaskbar;

    m_TaskMain = new TaskMainWidget;
    m_screenLogin = new TaskLoginScreen(this);
    m_screenLoading = new TaskLoadingScreen(this);
    m_statusBar = new QStatusBar;
    m_panel = new QStackedLayout;

    readSettings();

    dbox->setSpacing(0);
    dbox->setMargin(0);
    dbox->addWidget(m_taskBar);
    dbox->addWidget(m_TaskMain);

    m_mainUI->setLayout(dbox);

    m_states[TASK_STATE_INIT].id = -1;
    m_states[TASK_STATE_LOGIN].id = m_panel->addWidget(m_screenLogin);
    m_states[TASK_STATE_LOADING].id = m_panel->addWidget(m_screenLoading);
    m_states[TASK_STATE_RUNNING].id = m_panel->addWidget(m_mainUI);
    m_states[TASK_STATE_CLOSE].id = -1;

    m_states[TASK_STATE_INIT].state = NULL;
    m_states[TASK_STATE_LOGIN].state = dynamic_cast<GlobalState *>(m_screenLogin);
    m_states[TASK_STATE_LOADING].state = dynamic_cast<GlobalState *>(m_screenLoading);
    m_states[TASK_STATE_RUNNING].state = dynamic_cast<GlobalState *>(m_mainUI);
    m_states[TASK_STATE_CLOSE].state = NULL;

    m_state = TASK_STATE_FIRST;
    m_panel->setContentsMargins(0, 0, 0, 0);
    m_panel->setSpacing(0);

    // make connections
    connect(m_screenLogin, SIGNAL(login(QString,QString)), this, SLOT(onLogin(QString,QString)));

    panelWidget->setLayout(m_panel);

    setTaskState(TASK_STATE_LOGIN);
    setStatusBar(m_statusBar);

    setWindowTitle(tr("Tasks"));
    setUnifiedTitleAndToolBarOnMac(true);
    setCentralWidget(panelWidget);
}

void TaskMainWindow::handleLogin(QXmlStreamReader *xml)
{
    bool login_begin = false;
    QString tag;

    int id = USERDATA_ID_INVALID;

    while (!xml->atEnd()) {
        xml->readNext();

        if (xml->isStartElement()) {
            tag = xml->name().toString();

            if (tag == "loginvalid") {
                login_begin = true;
            }
        } else if (xml->isEndElement()) {
            if (xml->name() == "loginvalid") {
                if (login_begin == true) {
                    // Ok!
                    login_begin = false;
                } else {
                    // invalid login
                }
            }
        } else if ((xml->isCharacters()) &&
                   (!xml->isWhitespace())) {
            if (login_begin == true) {
                if (tag == "id") {
                    bool success;
                    int t = xml->text().toString().toInt(&success);

                    if (success) { id = t; }
                } else if (tag == "sessionid") {
                    if (!xml->text().toString().isEmpty()) {
                        TaskGlobal::setSessionId(xml->text().toString());
                    } else {
                        // invalid session id
                    }
                }
            } else {
                // ignore other shit that may be here
            }
        }
    }

//    m_screenLogin->setButtonEnabled(true);

    if (id != USERDATA_ID_INVALID){
        m_data = new DataUser(id);

        m_screenLoading->loadUserData(id);
    } else {
        m_screenLoading->displayError("Login invalid!");
    }
}

void TaskMainWindow::handleUserData(QXmlStreamReader *xml)
{
//    QMessageBox msgBox;
//    msgBox.setText(data);
//    msgBox.exec();
    QString tag;
    bool userflag = false;
    bool success;
    bool error = false;
    bool prefflag = false;

    int id;
    QString szUsername;
    QString szCompany;
    QString szEmail;
    QString szName;
    DataPreferences p;

    while (!xml->atEnd()) {
        xml->readNext();

        if (xml->isStartElement()) {
            tag = xml->name().toString();

            if (tag == "user") { userflag = true; }
            else if (tag == "pref") { prefflag = true; }
        } else if (xml->isEndElement()) {
            if (xml->name() == "user") { userflag = false; }
            else if (xml->name() == "pref") { prefflag = false; }
        } else if ((xml->isCharacters()) &&
                   (!xml->isWhitespace())) {
            if (tag == "error")
            {
                // handle error
                error = true;
            } else if (userflag == true) {
                if (tag == "id") {
                    id = xml->text().toString().toInt(&success);

                    if (!success) { id = USERDATA_ID_INVALID; }
                } else if (tag == "username") {
                    szUsername = xml->text().toString();
                } else if (tag == "email") {
                    szEmail = xml->text().toString();
                } else if (tag == "name") {
                    szName = xml->text().toString();
                } else if (tag == "company") {
                    szCompany = xml->text().toString();
                } else if (prefflag == true) {
                    p.setValue(tag, QVariant(xml->text().toString()));
                }
            }
        }
    }

    if ((!xml->hasError()) &&
        (!error)) {
        // id sanity check
        if (id == m_data->getId()) {
            m_data->setCompany(szCompany);
            m_data->setName(szName);
            m_data->setEmail(szEmail);
            m_data->setUsername(szUsername);
            m_data->setPrefernces(p);

            TaskGlobal::setUserData(*m_data);
        }

        TaskGlobal::rawTaskQuery(this, TaskGlobal::PostDataMap(), qCNT);

#ifdef CUSTOM_TASKS
        Task *t;

        t = TaskGlobal::newTask(TaskGlobal::getUserData().getId());
        t->setDescription("make it work biatch!  this is just a basic text description");
        t->setTitle("Finish basic task input");
        t->setColor(QColor(11, 95, 165));
        m_TaskMain->getTaskList()->addTask(*t);

        t = TaskGlobal::newTask(TaskGlobal::getUserData().getId());
        t->setDescription("It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to Heaven, we were all going direct the other way - in short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only.");
        t->setTitle("A Tale of Two Cities by Charles Dickens");
        t->setColor(QColor(95, 11, 165));
        m_TaskMain->getTaskList()->addTask(*t);

        for (int i = 0; i < 5; i++) {
            t = TaskGlobal::newTask(TaskGlobal::getUserData().getId());
            QString sn;
            sn.sprintf("Task %d", t->getId());

            t->setTitle(sn);
            t->setDescription("This is just a test task");
            t->setColor(QColor(0x57, 0xbf, 0x40));
            m_TaskMain->getTaskList()->addTask(*t);
        }

        for (int i = 1; i <= 5; i++) {
            t = TaskGlobal::newTask(TaskGlobal::getUserData().getId());
            QString sn;
            sn.sprintf("Extra Task %d", i);

            t->setTitle(sn);
            t->setDescription("This is an extra task, the text here does not matter");
//            t->setColor(QColor(0x57, 0xbf, 0x40));
            m_TaskMain->getTaskList()->addTask(*t);
        }

        t = TaskGlobal::getTask(5);
        if (t) { t->complete(); }
#endif
    } else {

    }
}

void TaskMainWindow::handleCounts(QXmlStreamReader *xml)
{
    QString tag;
    bool countFlag = false;

    int taskcnt = 0;
    int usercnt = 0;

    while (!xml->atEnd()) {
        xml->readNext();
        if (xml->isStartElement()) {
            tag = xml->name().toString();

            if (xml->name() == "count") { countFlag = true; }
        } else if (xml->isEndElement()) {
            if (xml->name() == "count") { countFlag = false; }
        } else if ((xml->isCharacters()) &&
                   (!xml->isWhitespace())) {
            if (tag == "user") {
                usercnt = xml->text().toString().toInt();
            } else if (tag == "task") {
                taskcnt = xml->text().toString().toInt();
            }
        }
    }

    if (xml->hasError()) {

    } else {
        // successful read
        qDebug() << "taskcnt= " << taskcnt << "  usercnt= " << usercnt;
        m_screenLoading->initLoadData(taskcnt, usercnt);

        TaskGlobal::rawTaskQuery(this, TaskGlobal::PostDataMap(), qUSERS);
    }
}

void TaskMainWindow::handleUsers(QXmlStreamReader *xml)
{
    QString tag;
    bool userFlag = false;
    bool userDataFlag = false;
    DataUser *cur = NULL;

    while (!xml->atEnd()) {
        xml->readNext();
        if (xml->isStartElement()) {
            tag = xml->name().toString();

            if (xml->name() == "userdata") { userDataFlag = true; }
            if (xml->name() == "user") { userFlag = true; }
        } else if (xml->isEndElement()) {

            if (xml->name() == "userdata") { userDataFlag = false; }
            if (xml->name() == "user") {
                userFlag = false;

                if (cur != NULL) {
                    // add user
                    TaskGlobal::addUser(*cur);
                    m_screenLoading->loadedUser();

                    delete cur;
                    cur = NULL;
                }
            }
        } else if ((xml->isCharacters()) &&
                   (!xml->isWhitespace())) {
            if ((userDataFlag) && (userFlag)) {
                if (tag == "id") {
                    int id = xml->text().toString().toInt();
                    if (cur == NULL) { cur = new DataUser(id); }
                } else if (tag == "username") {
                    if (cur != NULL) { cur->setUsername(xml->text().toString()); }
                } else if (tag == "email") {
                    if (cur != NULL) { cur->setEmail(xml->text().toString()); }
                } else if (tag == "name") {
                    if (cur != NULL) { cur->setName(xml->text().toString()); }
                } else if (tag == "company") {
                    if (cur != NULL) { cur->setCompany(xml->text().toString()); }
                }
            }
        }
    }

    if (xml->hasError()) {

    } else {
        qDebug() << "Added " << TaskGlobal::getNumUsers() << " users";

        TaskGlobal::rawTaskQuery(this, TaskGlobal::PostDataMap(), qTASKS);
    }
}

void TaskMainWindow::handleTaskData(QXmlStreamReader *xml)
{
    QString tag;

    while (!xml->atEnd()) {
        xml->readNext();

        if (xml->isStartElement()) {
            tag = xml->name().toString();
        } else if (xml->isEndElement()) {
        } else if ((xml->isCharacters()) &&
                   (!xml->isWhitespace())) {
        }
    }

    if (xml->hasError()) {

    } else {

    }
}

void TaskMainWindow::handleTasks(QXmlStreamReader *xml)
{
    QString tag;
    QXmlStreamAttributes attr;

    bool tdataFlag = false;
    bool taskFlag = false;
    bool eventFlag = false;
    Task *t = NULL;

    bool haveId = false;
    int id, owner;
    QString title, desc;
    Task::TaskEventList el;
    QColor c;
    QDateTime mod;

    while (!xml->atEnd()) {
        xml->readNext();

        if (xml->isStartElement()) {
            tag = xml->name().toString();
            attr = xml->attributes();

            if (xml->name() == "taskdata") { tdataFlag = true; }
            if (xml->name() == "task") { taskFlag = true; }
            if (xml->name() == "events") { eventFlag = true; }
        } else if (xml->isEndElement()) {
            if (xml->name() == "taskdata") { tdataFlag = false; }
            if (xml->name() == "events") { eventFlag = false; }
            if (xml->name() == "task") {
                taskFlag = false;
                haveId = false;
//                haveCreator = false;

                // save task
                if (t != NULL) {
                    t->setTitle(title);
                    t->setDescription(desc);
                    t->setOwner(owner);
                    t->setColor(c);
                    t->addEvents(el);

                    owner = 0;
                    title.clear();
                    desc.clear();
                    el.clear();
                    mod = QDateTime();
                    c = QColor(0, 0, 0, 0);

                    m_screenLoading->loadedTask();

                    TaskGlobal::loadTask(*t);

                    delete t;
                    t = NULL;
                }
            }

        } else if ((xml->isCharacters()) &&
                   (!xml->isWhitespace())) {

            if ((tdataFlag) && (taskFlag)) {
                // task tags
                if (tag == "id") {
                    id = xml->text().toString().toInt(&haveId);
                } else if (tag == "owner") {
                    owner = xml->text().toString().toInt();
                } else if (tag == "title") {
                    title = xml->text().toString();
                } else if (tag == "description") {
                    desc = xml->text().toString();
                } else if (tag == "color") {
                    c = colorFromHexString(xml->text().toString());
                } else if (tag == "modifiedtime") {
                    mod = QDateTime::fromString(xml->text().toString(), "yyyy-MM-dd HH:mm::ss");
                }

                if ((t == NULL) && (haveId)) {
                    t = new Task(id);
                }

                if (eventFlag) {
                    if (tag == "event") {
                        // taskevent tags
                        TaskEvent::TaskEventType type = TaskEvent::CREATED;
                        int cid = USERDATA_ID_INVALID;

                        for (int i = 0; i < attr.size(); i++) {
                            if (attr[i].name() == "creator") {
                                // id of event creator
                                cid = attr[i].value().toString().toInt();
                            } else if (attr[i].name() == "type") {
                                if (attr[i].value() == "CREATED") { type = TaskEvent::CREATED; }
                                if (attr[i].value() == "REMOVED") { type = TaskEvent::REMOVED; }
                                if (attr[i].value() == "COMPLETED") { type = TaskEvent::COMPLETED; }
                                if (attr[i].value() == "STARTED") { type = TaskEvent::STARTED; }
                                if (attr[i].value() == "STOPPED") { type = TaskEvent::STOPPED; }
                                if (attr[i].value() == "SCHEDULED") { type = TaskEvent::SCHEDULED; }
                            }
                        }

                        // time xml->text();
                        QDateTime tm = QDateTime::fromString(xml->text().toString(), "yyyy-MM-dd hh:mm:ss");

                        TaskEvent e;
                        e.setTime(tm);
                        e.setType(type);
                        e.setUser(cid);
                        el.push_back(e);
                    }
                }

                // done?
            }
        }
    }

    if (xml->hasError()) {

    } else {
        qDebug() << "Added " << TaskGlobal::getNumTasks() << " tasks";

        m_TaskMain->setupTaskList();

        // this code should be called last
        m_taskBar->setUserData(TaskGlobal::getUserData());
        setTaskState(TASK_STATE_RUNNING);
    }
}

void TaskMainWindow::handleLinks(QXmlStreamReader *xml)
{

}

void TaskMainWindow::setTaskState(const TaskStates& state)
{
    if ((state >= TASK_STATE_FIRST) && (state < TASK_STATE_LAST)) {
        if ((state != m_state) &&
            (m_states[state].id != -1)) {
            //setCentralWidget(m_states[state].m_taskWidget);

            if (m_states[m_state].state != NULL) { m_states[m_state].state->endState(); }

            m_panel->setCurrentIndex(m_states[state].id);

            if (m_states[state].state != NULL) { m_states[state].state->beginState(); }

            m_state = state;
        }
    }
}

void TaskMainWindow::writeSettings()
{
    m_settings.beginGroup("MainWindow");
    m_settings.setValue("state", static_cast<int>(windowState()));
    if (windowState() != Qt::WindowMaximized) {
        m_settings.setValue("size", size());
        m_settings.setValue("pos", pos());
    }
    m_settings.endGroup();
}

void TaskMainWindow::readSettings()
{
    m_settings.beginGroup("MainWindow");
    if (m_settings.value("state").toInt() == Qt::WindowMaximized)
    {
        setWindowState(Qt::WindowMaximized);
    } else {
        resize(m_settings.value("size").toSize());
        move(m_settings.value("pos").toPoint());
    }
    m_settings.endGroup();
}

void TaskMainWindow::closeEvent(QCloseEvent *event)
{
//    if (askQuit() == true) {
        event->accept();

        writeSettings();
//    } else {
//        event->ignore();
//    }
}

void TaskMainWindow::createActions()
{
    QAction *a;

    a = new QAction("Preferences", this);
    a->setShortcut(QKeySequence(Qt::ALT + Qt::Key_P));
    TaskGlobal::setAction("Preferences", a);

    a = new QAction("Import Tasks", this);
    a->setShortcut(QKeySequence(Qt::ALT + Qt::Key_I));
    TaskGlobal::setAction("ImportTasks", a);

    a = new QAction("Logout", this);
    a->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_L));
    TaskGlobal::setAction("Logout", a);
    connect(a, SIGNAL(triggered()), this, SLOT(onLogout()));

    a = new QAction("Quit", this);
    a->setShortcut(QKeySequence::Quit);
    TaskGlobal::setAction("Quit", a);
    connect(a, SIGNAL(triggered()), TaskGlobal::getApp(), SLOT(quit()));
}

void TaskMainWindow::onPreferences()
{

}

void TaskMainWindow::onLogout()
{
    QMessageBox b;
    b.setIcon(QMessageBox::Question);
    b.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    b.setText("Are you sure you want to logout?");

    if (b.exec() == QMessageBox::Yes) {
        TaskGlobal::PostDataMap m;
        m.insert("sess", TaskGlobal::getSessionId());
        TaskGlobal::rawTaskQuery(this, m, qLOGOUT);

        setTaskState(TASK_STATE_LOGIN);
    }
}

void TaskMainWindow::onImportTasks()
{

}

void TaskMainWindow::onLogin(const QString& username, const QString& password)
{
    // set loading screen message
    m_screenLoading->setLabel("Logging in...", QIcon(":/images/16x16/key_go.png"));

    qDebug() << "Logging in...";

    // set state loading
    setTaskState(TASK_STATE_LOADING);

    repaint();

    TaskGlobal::login(this, username, password);
}

