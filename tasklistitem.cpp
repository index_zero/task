#include "tasklistitem.h"
#include "taskglobal.h"
#include "task.h"
#include <QPainter>

TaskListItem::TaskListItem(const int& taskid)
{
    m_taskid = taskid;

    m_taskPointer = TaskGlobal::getTask(taskid);

    m_itemWidth = 420;
    m_boundingRect.setCoords(0, 0, m_itemWidth, 60);

    setFlags(ItemIsSelectable);// | ItemIsMovable);
    setAcceptHoverEvents(true);

    m_highlight = false;
    m_keepHighlight = false;
}

QRectF TaskListItem::boundingRect() const
{
    return m_boundingRect;
}

QPainterPath TaskListItem::shape() const
{
    QPainterPath path;
    path.addRect(m_boundingRect);
    return path;
}

void TaskListItem::setItemWidth(const int& width)
{
    if (width >= 420) {
        prepareGeometryChange();
        m_boundingRect.setWidth(width);
        m_itemWidth = width;
    }
}

void TaskListItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    // if we are highlighted, draw that - otherwise draw separator
    m_highlight = false;

    if (option->state & QStyle::State_Selected) {
        drawHighlight(painter);

        m_highlight = true;
    } else {
        drawSeparator(painter);

        if (option->state & QStyle::State_MouseOver) {
            m_highlight = true;
        } else if (m_keepHighlight == true) {
            m_highlight = true;
        } else {
            m_highlight = false;
        }
    }

    drawTaskColor(painter, m_highlight);

    drawTitle(painter);
    drawOwner(painter);
    drawDescription(painter);

    drawIcons(painter);
    drawTime(painter);

    Q_UNUSED(widget);
}

void TaskListItem::drawHighlight(QPainter *painter)
{
    QColor border(QColor(0x0b, 0x61, 0xa4));
    QColor highlight(QColor(0x66, 0xa3, 0xd2));
    QRectF rect;

    highlight = highlight.lighter(160);

    QBrush oldbrush;
    QBrush hlbrush(highlight);

    QPen oldpen;
    QPen hlpen(border);
    hlpen.setStyle(Qt::SolidLine);

    // save, set pen
    oldpen = painter->pen();
    painter->setPen(hlpen);

    // save, set brush
    oldbrush = painter->brush();
    painter->setBrush(hlbrush);

    rect.setRect(m_boundingRect.left(),
                 m_boundingRect.top() + 1,
                 m_boundingRect.width(),
                 m_boundingRect.height() - 4);

    painter->drawRect(rect);

    // revert
    painter->setPen(oldpen);
    painter->setBrush(oldbrush);
}

void TaskListItem::drawTitle(QPainter *painter)
{
    const int width_color = SIZE_BORDER + SIZE_SPACER + SIZE_COLOR;
    const int width_buttons = SIZE_BORDER + SIZE_SPACER + SIZE_BUTTONS;
    const int width_text = m_boundingRect.width() - width_color - width_buttons;

    QFont oldfont;
    QFont fnt("Verdana", 11);
    int offset = 0;

    if (m_taskPointer->isCompleted()) {
        fnt.setStrikeOut(true);

        QIcon *icon = new QIcon(":/images/16x16/tick.png");
        QPixmap pixmap = icon->pixmap(QSize(16, 16), QIcon::Normal, QIcon::On);

        painter->drawPixmap(width_color, SIZE_BORDER + ((SIZE_TITLE - 16) / 2), pixmap);
        offset = 20;
    }

    // save, set
    oldfont = painter->font();
    painter->setFont(fnt);

    painter->drawText(width_color + offset, SIZE_BORDER, width_text - offset, SIZE_TITLE,
                      Qt::AlignLeft | Qt::AlignVCenter, m_taskPointer->getTitle());

    // revert
    painter->setFont(oldfont);
}

void TaskListItem::drawOwner(QPainter *painter)
{
    const int width_color = SIZE_BORDER + SIZE_SPACER + SIZE_COLOR;
    const int width_buttons = SIZE_BORDER + SIZE_SPACER + SIZE_BUTTONS;
    const int width_text = m_boundingRect.width() - width_color - width_buttons;

//    const int height = m_boundingRect.height() - (2 * SIZE_SPACER + 1) - SIZE_TITLE - SIZE_OWNER;

    QFont oldfont;
    QFont fnt("Verdana", 8);
    fnt.setBold(true);

    // save, set
    oldfont = painter->font();
    painter->setFont(fnt);
    int owner = m_taskPointer->getOwner();

    painter->drawText(width_color, SIZE_BORDER + SIZE_TITLE, width_text, SIZE_OWNER,
                      Qt::AlignLeft | Qt::AlignVCenter,
                      TaskGlobal::getUser(owner).getName());

    // revert
    painter->setFont(oldfont);
}

void TaskListItem::drawDescription(QPainter *painter)
{
    const int width_color = SIZE_BORDER + SIZE_SPACER + SIZE_COLOR;
    const int width_buttons = SIZE_BORDER + SIZE_SPACER + SIZE_BUTTONS;
    const int width_text = m_boundingRect.width() - width_color - width_buttons;

    const int height = m_boundingRect.height() - (2 * SIZE_SPACER + 1) - SIZE_TITLE - SIZE_OWNER;

    QFont oldfont;
    QFont fnt("Verdana", 8);
    QPen pen(QColor(96, 96, 96));
    QPen oldpen;
    fnt.setBold(false);

    // save, set
    oldfont = painter->font();
    painter->setFont(fnt);

    // save pen, set pen
    oldpen = painter->pen();
    painter->setPen(pen);

    painter->drawText(width_color, SIZE_BORDER + SIZE_TITLE + SIZE_OWNER, width_text, height,
                      Qt::AlignLeft | Qt::AlignVCenter, m_taskPointer->getDescription());

    // revert
    painter->setFont(oldfont);
    painter->setPen(oldpen);
}

void TaskListItem::drawTaskColor(QPainter *painter, bool highlight)
{
    QPen nopen(Qt::NoPen);
    QPen oldpen;
    QBrush oldbrush;
    QBrush colorBrush(m_taskPointer->getColor());
    int width = (highlight ? SIZE_COLOR : SIZE_HANDLE);

    // save pen, set pen
    oldpen = painter->pen();
    painter->setPen(nopen);

    // save brush, set brush
    oldbrush = painter->brush();
    painter->setBrush(colorBrush);

    // draw rect (2 from left edge, 3 from bottom, 2 from top, 5 wide)
    painter->drawRect(SIZE_BORDER, SIZE_BORDER, width,
                      m_boundingRect.height() - (2 * SIZE_BORDER + 1));

    // revert pen & brush
    painter->setBrush(oldbrush);
    painter->setPen(oldpen);
}

void TaskListItem::drawSeparator(QPainter *painter)
{
    QPen pen;
    QPen oldpen;

    pen.setStyle(Qt::SolidLine);
    pen.setColor(QColor(192, 192, 192));

    // save pen, set pen
    oldpen = painter->pen();
    painter->setPen(pen);

    painter->drawLine(1, m_boundingRect.height() - 1,
                      m_boundingRect.width() - 2, m_boundingRect.height() - 1);

    // revert pen
    painter->setPen(oldpen);
}

void TaskListItem::drawIcons(QPainter *painter)
{
    Q_UNUSED(painter);
}

void TaskListItem::drawTime(QPainter *painter)
{
    const int width_color = SIZE_BORDER + SIZE_SPACER + SIZE_COLOR;
    const int width_buttons = SIZE_BORDER + SIZE_SPACER + SIZE_BUTTONS;
    const int width_text = m_boundingRect.width() - width_color - width_buttons;

    const int height = m_boundingRect.height() - (2 * SIZE_SPACER + 1) - SIZE_TITLE - SIZE_OWNER;

    QFont oldfont;
    QFont fnt("Verdana", 8);
    fnt.setBold(true);

    QPen pen = painter->pen();
    QPen oldpen;

    // save, set
    oldfont = painter->font();
    painter->setFont(fnt);

    QDateTime t = m_taskPointer->getLastModified();

    if (m_taskPointer->isCompleted()) {
        if (m_taskPointer->getNumEvents(TaskEvent::COMPLETED) > 0) {
            t = m_taskPointer->getFirstEvent(TaskEvent::COMPLETED).getTime();
        }

        pen.setColor(QColor(0x57, 0xbf, 0x40));
        pen.setStyle(Qt::SolidLine);
    } else {
        if (m_taskPointer->getNumEvents(TaskEvent::CREATED) > 0) {
            t = m_taskPointer->getFirstEvent(TaskEvent::CREATED).getTime();
        }
    }

    // save pen, set pen
    oldpen = painter->pen();
    painter->setPen(pen);

    DataPreferences perf = TaskGlobal::getUserData().getPreferences();

    // draw Date
    painter->drawText(width_color + width_text + SIZE_SPACER, SIZE_BORDER + SIZE_TITLE, SIZE_BUTTONS, SIZE_OWNER,
                      Qt::AlignRight | Qt::AlignVCenter,
                      t.toString(perf.value("dateformat").toString()));
    // draw Time
    painter->drawText(width_color + width_text + SIZE_SPACER, SIZE_BORDER + SIZE_TITLE + SIZE_OWNER, SIZE_BUTTONS, height,
                      Qt::AlignRight | Qt::AlignVCenter,
                      t.toString(perf.value("timeformat").toString()));

    // revert
    painter->setFont(oldfont);
    painter->setPen(oldpen);
}

void TaskListItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    update();
}

void TaskListItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseMoveEvent(event);
    update();
}

void TaskListItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseReleaseEvent(event);
    update();
}

void TaskListItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mouseDoubleClickEvent(event);
    update();
}
