#include <QDateTime>
#include <QHBoxLayout>
#include <QPixmap>
#include <QMenu>
#include <QDesktopServices>

#include "desktoptaskbar.h"
#include "taskglobal.h"

DesktopTaskbar::DesktopTaskbar(QWidget *parent) :
    QLabel(parent)
{
    m_Weather = new QPushButton;
    m_DateTime = new QLabel;
    m_Notifications = new QLabel;
    m_User = new QPushButton;
    QHBoxLayout *hbox = new QHBoxLayout;

    m_prefs.setValue("zipcode", QVariant("10007"));
    m_prefs.setValue("weather_update_freq", QVariant("3600"));

    setObjectName("desktoptaskbar");
    setStyleSheet("#desktoptaskbar {"
        "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1,"
            "stop:0 #404040,stop:0.5 #000000,stop:1 #404040);"
        "margin-bottom: 2px;"
        "color: #cfcfcf; }");
    setAlignment(Qt::AlignVCenter);
    setMaximumHeight(32);
    setMinimumHeight(32);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    m_DateTime->setStyleSheet("background-color: transparent; color: #cfcfcf;");
    m_Weather->setStyleSheet("#weatherunderground { background-color: transparent; }");
    m_Notifications->setStyleSheet("background-color: transparent; color: #cfcfcf;");
    m_User->setStyleSheet("background-color: rgba(0, 0, 0, 0% );"
                          "font-weight: bold;"
                          "color: #cfcfcf;");
    m_User->setFlat(true);
//    m_User->setAttribute(Qt::WA_TranslucentBackground);
//    m_User->setWindowFlags(Qt::FramelessWindowHint);

    m_DateTime->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);
    m_Notifications->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_User->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

    m_Weather->setIconSize(QSize(16, 16));
    m_Weather->setFlat(true);
    m_Weather->setObjectName("weatherunderground");
    m_Weather->setStyleSheet("#weatherunderground { background-color: transparent; }");
    m_Weather->setCursor(QCursor(Qt::PointingHandCursor));
//    m_Weather->set

    connect(m_Weather, SIGNAL(clicked()), this, SLOT(showForecast()));

    // http://api.wunderground.com/api/58975428570aa6e5/conditions/q/<zip code>.xml
    //updateWeather(m_prefs.value("zipcode").toString());
    //getUrl(QUrl("http://theindexzero.com/tasks/dbtest.php"));

    QDateTime time = QDateTime::currentDateTime();
    QString ttext;

    ttext.append("<b>");
    ttext.append(time.toString("dddd, MMMM d, yyyy h:mm ap"));
    ttext.append("</b>");

    m_Timer = 0;
    startTimer(1000);
    m_DateTime->setText(ttext);

    QMenu *menu = new QMenu;
    menu->addAction(TaskGlobal::getAction("Preferences"));
    menu->addAction(TaskGlobal::getAction("ImportTasks"));
    menu->addSeparator();
    menu->addAction(TaskGlobal::getAction("Logout"));

    m_User->setMenu(menu);

    m_User->setVisible(false);

    hbox->setMargin(2);
    hbox->setSpacing(2);
    hbox->addWidget(m_Weather);
    hbox->addWidget(m_DateTime);
    hbox->addWidget(m_Notifications);
    hbox->addWidget(m_User);
//    hbox->addWidget(m_Logout);

    setLayout(hbox);
}

void DesktopTaskbar::timerEvent(QTimerEvent *event)
{
    QDateTime time = QDateTime::currentDateTime();
    QString ttext;

    ttext.append("<b>");
    ttext.append(time.toString("dddd, MMMM d, yyyy h:mm ap"));
    ttext.append("</b>");

    m_DateTime->setText(ttext);

    m_Timer++;

    if (m_Timer >= m_prefs.value("weather_update_freq").toString().toInt()) {
        updateWeather(m_prefs.value("zipcode").toString());
        m_Timer = 0;
    }

    Q_UNUSED(event);
}

void DesktopTaskbar::updateWeather(const QString& zip)
{
    QString sz;

    sz.append("http://api.wunderground.com/api/58975428570aa6e5/conditions/q/");
    sz.append(zip.mid(0, 5));
    sz.append(".xml");

    TaskGlobal::loadWeather(this, sz);
}

void DesktopTaskbar::handleWeatherData(QXmlStreamReader *xml)
{
    QString tag, url;
    QStringList forecast;

    while (!xml->atEnd())
    {
        xml->readNext();

        if (xml->isStartElement())
        {
            tag = xml->name().toString();
        }
        else if (xml->isEndElement())
        {
            if (xml->name() == "current_observation")
            {
                m_Weather->setToolTip(forecast.join("\n"));
            }
        }
        else if ((xml->isCharacters()) &&
                 (!xml->isWhitespace()))
        {
            if (tag == "weather")
            {
                forecast << xml->text().toString();
            }
            else if (tag == "temperature_string")
            {
                QString sz("Temp: ");
                sz.append(xml->text());
                forecast << sz;
            }
            else if (tag == "wind_string")
            {
                QString sz("Wind: ");
                sz.append(xml->text());
                forecast << sz;
            }
            else if (tag == "dewpoint_string")
            {
                QString sz("Dewpoint: ");
                sz.append(xml->text());
                forecast << sz;
            }
            else if (tag == "icon")
            {
                setWeatherIcon(xml->text().toString());
            }
            else if (tag == "forecast_url")
            {
                m_ForecastLink.setUrl(xml->text().toString());
            }
        }
    }
}

void DesktopTaskbar::setWeatherIcon(const QString& iconname)
{
    if ((iconname == "chancesnow") ||
        (iconname == "snow") ||
        (iconname == "flurries"))
    {
        m_Weather->setIcon(QIcon(":/images/small/weather_snow.png"));
    }
    if ((iconname == "chanceflurries") ||
        (iconname == "chancesleet") ||
        (iconname == "sleet"))
    {
        m_Weather->setIcon(QIcon(":/images/small/snow_rain.png"));
    }
    else if ((iconname == "chancerain") ||
             (iconname == "rain"))
    {
        m_Weather->setIcon(QIcon(":/images/small/weather_rain.png"));
    }
    else if ((iconname == "chancetstorms") ||
             (iconname == "tstorms"))
    {
        m_Weather->setIcon(QIcon(":/images/small/weather_lightning.png"));
    }
    else if ((iconname == "clear") ||
             (iconname == "sunny"))
    {
        m_Weather->setIcon(QIcon(":/images/small/weather_sun.png"));
    }
    else if ((iconname == "fog") ||
             (iconname == "hazy"))
    {
        m_Weather->setIcon(QIcon(":/images/small/fog.png"));
    }
    else if ((iconname == "partlycloudy") ||
             (iconname == "mostlysunny"))
    {
        m_Weather->setIcon(QIcon(":/images/small/weather_cloudy.png"));
    }
    else if ((iconname == "partlysunny") ||
             (iconname == "mostlycloudy"))
    {
        m_Weather->setIcon(QIcon(":/images/small/sun_cloudy.png"));
    }
    else
    {
        m_Weather->setIcon(QIcon(":/images/small/weather_clouds.png"));
    }
}

void DesktopTaskbar::setUserData(const DataUser& data)
{
    m_data = data;

    QString name;

    name.append(data.getName());
    name.append(" (");
    name.append(data.getUsername());
    name.append(")");
    m_prefs = data.getPreferences();

    updateWeather(m_prefs.value("zipcode").toString());
    m_User->setText(name);
    m_User->setVisible(true);
}

void DesktopTaskbar::showForecast()
{
    QDesktopServices::openUrl(m_ForecastLink);
}
