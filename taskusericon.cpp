#include "taskusericon.h"

#include <QVBoxLayout>
#include <QIcon>

TaskUserIcon::TaskUserIcon(QWidget *parent) :
    QLabel(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout;

    setStyleSheet("QLabel {"
        "border-radius: 10px;"
        "border: 1px solid black;"
        "background-color: #404040;"
        "color: #ffffff; }");
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setMinimumHeight(72);
    setMinimumWidth(72);
    setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    m_Picture = new QLabel;
    m_Name = new QLabel;

    m_Picture->setStyleSheet("border: 0px;");
    m_Name->setStyleSheet("border: 0px;");

    QIcon *icon = new QIcon(":/images/user.png");
    QPixmap pixmap = icon->pixmap(QSize(32, 32), QIcon::Normal, QIcon::On);

    m_Picture->setPixmap(pixmap);
    m_Picture->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
    m_Picture->setMinimumSize(QSize(32,32));
    m_Picture->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

    m_Name->setText("Stephen\nSchweizer");
    m_Name->setAlignment(Qt::AlignHCenter);

    vbox->setSpacing(0);
    vbox->setMargin(2);
    vbox->addWidget(m_Picture);
    vbox->addWidget(m_Name);

    setLayout(vbox);
}

void TaskUserIcon::setDragTarget()
{
    QIcon *icon = new QIcon(":/images/user_add.png");
    QPixmap pixmap = icon->pixmap(QSize(32, 32), QIcon::Disabled, QIcon::On);

    m_Picture->setPixmap(pixmap);
    m_Name->setText("Drag to Add");
}
