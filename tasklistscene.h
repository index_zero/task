#ifndef TASKLISTSCENE_H
#define TASKLISTSCENE_H

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QPushButton>
#include <QGraphicsProxyWidget>

class TaskListScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit TaskListScene(QObject *parent = 0);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);
//    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    void add(QGraphicsItem *item);
    void remove(QGraphicsItem *item);
    void removeAll();
    int getItemIndex(QGraphicsItem *item);

    void updateScene();
    void updateRect(const QRectF& rect);

protected:
    void redraw();

    void removeAllItems();

private:
    QGraphicsItem *m_lastItem;
    QGraphicsProxyWidget *m_trashBtnProxy;
    QPushButton *m_trashBtn;

    QList<QGraphicsItem*> m_itemList;
    QMap<QGraphicsItem*, int> m_itemIndex;

signals:

public slots:
    //void onSceneRectChanges(const QRectF& rect);
};

#endif // TASKLISTSCENE_H
