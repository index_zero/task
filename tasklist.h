#ifndef TASKLIST_H
#define TASKLIST_H

#include <QWidget>

#include <QLineEdit>
#include <QListView>
#include <QDateTimeEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <QList>
#include <QGraphicsView>
#include <QGraphicsScene>

#include "tasklistitem.h"
#include "tasklistview.h"
#include "tasklistscene.h"

class TaskList : public QWidget
{
    Q_OBJECT
public:
    explicit TaskList(QWidget *parent = 0);

    void addTask(const int& taskid);

    void updateScene();

    void mouseMoveEvent(QMouseEvent *event);

signals:
    void taskChanged(const int& taskid);

public slots:
    void search();
    void updateResults(const QDateTime& newdate);
    void selectionChanged();

    void itemClicked(QListWidgetItem* item);
    void itemChanged(QListWidgetItem* item);
private:
//    TaskListView *m_TaskList;
    QLineEdit *m_SearchBar;
    QDateTimeEdit *m_DateEdit;

    QListWidget *m_listWidget;
    TaskListView *m_View;
    TaskListScene *m_Scene;

    QList<TaskListItem*> m_listItems;
};

#endif // TASKLIST_H
