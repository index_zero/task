#include "taskmainwidget.h"

//#include <QPlastiqueStyle>
#include "taskglobal.h"

TaskMainWidget::TaskMainWidget(QWidget *parent) :
    QWidget(parent)
{
    QHBoxLayout *hbox = new QHBoxLayout;
    QVBoxLayout *leftbox = new QVBoxLayout;
    QVBoxLayout *rightbox = new QVBoxLayout;
    QFrame *rightw = new QFrame;
    QFrame *leftw = new QFrame;

    m_splitter =  new QSplitter;

    leftw->setLayout(leftbox);
    leftw->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    rightw->setLayout(rightbox);
    rightw->setFrameStyle(QFrame::Panel | QFrame::Sunken);

    m_splitter->addWidget(leftw);
    m_splitter->addWidget(rightw);

    m_TaskList = new TaskList;

    setupViewPane();
    setupSecondaryView();

    leftbox->setContentsMargins(0, 0, 0, 0);
    rightbox->setContentsMargins(0, 0, 0, 0);
    leftbox->setSpacing(0);
    rightbox->setSpacing(0);

    leftbox->addWidget(m_viewPage);
    leftbox->addWidget(m_TaskList, 1);

    rightbox->addWidget(m_mainToolBar);
    rightbox->addLayout(m_secondaryLayout);

    connect(m_TaskList, SIGNAL(taskChanged(int)), m_panelDetail, SLOT(taskChanged(int)));

    m_splitter->setStretchFactor(0, 0);
    m_splitter->setStretchFactor(1, 1);
//    m_splitter->setCollapsible(0, true);
//    m_splitter->setCollapsible(1, false);
//    m_splitter->setStyle(new QPlastiqueStyle);
//    m_splitter->setFrameStyle(QFrame::Panel | QFrame::Sunken);

    hbox->setContentsMargins(0, 0, 0, 0);
    hbox->addWidget(m_splitter);

    setLayout(hbox);
}

void TaskMainWidget::setupViewPane()
{
    m_viewGroup = new QButtonGroup;
    m_viewPage = new QToolBar;

    m_addTaskBtn = new QToolButton;
    m_taskDetailBtn = new QToolButton;
    m_itineraryBtn = new QToolButton;
    m_calendarBtn = new QToolButton;

    m_addTaskBtn->setCursor(QCursor(Qt::ArrowCursor));
    m_addTaskBtn->setAutoRaise(true);
    m_addTaskBtn->setIconSize(QSize(32, 32));
    m_addTaskBtn->setText("Add Task");
    m_addTaskBtn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    m_addTaskBtn->setIcon(QIcon(":/images/wishlist_add.png"));

    m_calendarBtn->setCursor(QCursor(Qt::ArrowCursor));
    m_calendarBtn->setAutoRaise(true);
    m_calendarBtn->setIconSize(QSize(32, 32));
    m_calendarBtn->setText("View Calendar");
    m_calendarBtn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    m_calendarBtn->setIcon(QIcon(":/images/calendar.png"));

    m_itineraryBtn->setCursor(QCursor(Qt::ArrowCursor));
    m_itineraryBtn->setAutoRaise(true);
    m_itineraryBtn->setIconSize(QSize(32, 32));
    m_itineraryBtn->setText("View Itinerary");
    m_itineraryBtn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    m_itineraryBtn->setIcon(QIcon(":/images/legend.png"));

    m_taskDetailBtn->setCursor(QCursor(Qt::ArrowCursor));
    m_taskDetailBtn->setAutoRaise(true);
    m_taskDetailBtn->setIconSize(QSize(32, 32));
    m_taskDetailBtn->setText("View Task Detail");
    m_taskDetailBtn->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    m_taskDetailBtn->setIcon(QIcon(":/images/application_form_edit.png"));

    m_viewGroup->addButton(m_taskDetailBtn, svTASK_DETAIL);
    m_viewGroup->addButton(m_itineraryBtn, svITINERARY);
    m_viewGroup->addButton(m_calendarBtn, svCALENDAR);

    m_viewPage->addWidget(m_addTaskBtn);
    m_viewPage->addWidget(m_taskDetailBtn);
    m_viewPage->addWidget(m_itineraryBtn);
    m_viewPage->addWidget(m_calendarBtn);
}

void TaskMainWidget::setupTaskList()
{
    TaskGlobal::TaskList l = TaskGlobal::getTasks();

    for (int i = 0; i < l.size(); i++) {
        m_TaskList->addTask(l.at(i)->getId());

        if (i == 0) {
            m_panelDetail->taskChanged(l.at(i)->getId());
        }
    }

//    Task *t;

//    t = TaskGlobal::newTask(TaskGlobal::getUserData().getId());

//    t.setId(0);
//    t.setText(tr("Finish basic task input"));
//    t.setIcon(QIcon(":/images/messenger.png"));
//    t.addUser(tr("sschweizer"));
//    t.addUser(tr("phoffmann"));
//    t.addUser(tr("jfriewald"));

//    u.setId(1);
//    u.setText(tr("It was the best of times, it was the worst of times, it was the age of wisdom, it was the age of foolishness, it was the epoch of belief, it was the epoch of incredulity, it was the season of Light, it was the season of Darkness, it was the spring of hope, it was the winter of despair, we had everything before us, we had nothing before us, we were all going direct to Heaven, we were all going direct the other way - in short, the period was so far like the present period, that some of its noisiest authorities insisted on its being received, for good or for evil, in the superlative degree of comparison only."));
//    u.setIcon(QIcon(":/images/messenger.png"));
//    u.addUser(tr("sschweizer"));
//    u.addUser(tr("mcrowe"));

//    m_TaskList->addTask(t);
//    m_TaskList->addTask(u);
}

void TaskMainWidget::setupSecondaryView()
{
    m_mainToolBar = new QToolBar;
    m_mainToolBar->addAction(QIcon(":/images/16x16/wishlist_add.png"), "New Task");
    m_panelDetail = new TaskDetailPanel;

    m_secondaryLayout = new QStackedLayout;

    int p = m_secondaryLayout->addWidget(m_panelDetail);
    m_secondaryLayout->setCurrentIndex(p);
}

void TaskMainWidget::mouseMoveEvent(QMouseEvent *event)
{
    QWidget::mouseMoveEvent(event);
}
