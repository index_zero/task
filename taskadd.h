#ifndef TASKADD_H
#define TASKADD_H

#include <QLabel>

#include <QTextEdit>
#include <QComboBox>
#include <QTimeEdit>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QDateEdit>

class TaskAdd : public QLabel
{
    Q_OBJECT
public:
    explicit TaskAdd(QWidget *parent = 0);

signals:

public slots:
    void showAdvanced(bool show);

private:
    QTextEdit *m_TaskBody;
    QComboBox *m_User;
    QTimeEdit *m_ElapsedTime;
    QGroupBox *m_Advanced;

private:
    // advanced options
    QVBoxLayout *m_AdvancedOptions;
    QCheckBox *m_Milestone;
    QComboBox *m_Priority;
    QDateEdit *m_DueDate;
};

#endif // TASKADD_H
