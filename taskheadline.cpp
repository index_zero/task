#include <QHBoxLayout>

#include "taskheadline.h"

TaskHeadline::TaskHeadline(QWidget *parent) :
    QLabel(parent)
{
    QHBoxLayout *hbox = new QHBoxLayout;

    QIcon *icon = new QIcon(":/images/tick.png");
    m_Headline = new QLabel;
    m_Icon = new QLabel;
    QPixmap pixmap = icon->pixmap(QSize(32, 32), QIcon::Normal, QIcon::On);

    m_DismissButton = new QToolButton;
    m_DismissButton->setAutoRaise(true);
    m_DismissButton->setText(tr("Dismiss"));
    m_DismissButton->setIcon(QIcon(":/images/cross.png"));

    setStyleSheet("QLabel {"
        "border-radius: 3px;"
        "border: 1px solid black;"
        "background-color: #404040;"
        "color: #ffffff; }");
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    setMinimumHeight(38);   // 32 + 3 + 3

    m_Icon->setPixmap(pixmap);
    m_Icon->setStyleSheet("border: 0px solid black;");
    m_Headline->setStyleSheet("border: 0px solid black;");
    m_Headline->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_Headline->setText("<b>You have no tasks to complete today!</b>");
    m_Headline->setAlignment(Qt::AlignLeft | Qt::AlignVCenter);

    hbox->setMargin(1);
    hbox->setSpacing(5);
    hbox->addWidget(m_Icon);
    hbox->addWidget(m_Headline);
    hbox->addWidget(m_DismissButton);

    connect(m_DismissButton, SIGNAL(clicked(bool)), this, SLOT(setVisible(bool)));

    setLayout(hbox);
}

void TaskHeadline::setHeadlineText(const QString& text)
{
    m_Headline->setText(text);
}

void TaskHeadline::setHeadlineIcon(const QIcon& icon)
{
    QPixmap pixmap = icon.pixmap(QSize(32, 32), QIcon::Normal, QIcon::On);

    m_Icon->setPixmap(pixmap);
}
