#include "tasklistview.h"
#include "tasklistscene.h"

TaskListView::TaskListView(QWidget *parent) :
    QGraphicsView(parent)
{
    setEnabled(true);
    setMouseTracking(true);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void TaskListView::enterEvent(QEvent *event)
{
    QGraphicsView::enterEvent(event);

    TaskListScene *s = dynamic_cast<TaskListScene *>(scene());

    // update the scene when we enter
    if (s) { s->updateScene(); }
}

void TaskListView::leaveEvent(QEvent *event)
{
    QGraphicsView::leaveEvent(event);

    TaskListScene *s = dynamic_cast<TaskListScene *>(scene());

    // update the scene when we exit
    if (s) { s->updateScene(); }
}

void TaskListView::resizeEvent(QResizeEvent *event)
{
    QGraphicsView::resizeEvent(event);

    TaskListScene *s = dynamic_cast<TaskListScene *>(scene());

    QRectF rect;
    rect.setCoords(0, 0,
        event->size().width(),
        frameRect().height());

    // update the scene when we exit
    if (s) {
        s->updateScene();
        s->updateRect(rect);
        update();
        //s->updateWidth(rect.width());
    }
}
