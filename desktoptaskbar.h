#ifndef DESKTOPTASKBAR_H
#define DESKTOPTASKBAR_H

#include <QWidget>
#include <QLabel>
#include <QToolButton>
#include <QPushButton>

#include "global.h"
#include "datauser.h"

#define ONE_HOUR    (3600)

class DesktopTaskbar : public QLabel, public GlobalListener
{
    Q_OBJECT
public:
    explicit DesktopTaskbar(QWidget *parent = 0);

    void setUserData(const DataUser& data);

    void updateWeather(const QString& zip);

    void handleWeatherData(QXmlStreamReader *xml);
private:
    QPushButton *m_Weather;
    QLabel *m_DateTime;
    QLabel *m_Notifications;
    QPushButton *m_User;

    void getUrl(const QUrl& url);
    void updateWeatherFromXml();

    QUrl m_ForecastLink;

    void setWeatherIcon(const QString& iconname);

protected:
    void timerEvent(QTimerEvent *event);

    DataPreferences m_prefs;
    DataUser m_data;
    int m_Timer;

signals:

public slots:
    void showForecast();
};

#endif // DESKTOPTASKBAR_H
