#include "task.h"
#include "datauser.h"

#include <QDebug>

Task::Task(const int& id,
           const int& owner,
           const QString& title,
           const QString& desc)
{
    m_id = id;
    m_owner = owner;

    m_title = title;
    m_description = desc;

    setDefaults();
}

Task::Task(const int& id)
{
    m_id = id;
    m_owner = USERDATA_ID_INVALID;

    setDefaults();
}

Task& Task::operator=(const Task& t)
{
    if (this != &t) {
        m_lastModified  = t.m_lastModified;
        m_id            = t.m_id;
        m_color         = t.m_color;
        m_owner         = t.m_owner;
        m_title         = t.m_title;
        m_description   = t.m_description;

        for (int i = 0; i < TaskEvent::MAX_EVENTS; i++) {
            m_eventList[i] = t.m_eventList[i];
        }
    }

    return *this;
}

void Task::setDefaults()
{
    m_color = QColor(0, 0, 0, 0);   // transparent
    m_lastModified = QDateTime::currentDateTime();
}

void Task::setColor(const QColor& color)
{
    m_color = color;
    m_lastModified = QDateTime::currentDateTime();
}

void Task::setTitle(const QString& title)
{
    m_title = title;
    m_lastModified = QDateTime::currentDateTime();
}

void Task::setDescription(const QString& desc)
{
    m_description = desc;
    m_lastModified = QDateTime::currentDateTime();
}

void Task::setOwner(const int owner)
{
    m_owner = owner;
    m_lastModified = QDateTime::currentDateTime();
}

bool Task::isCompleted() const
{
    return false;
}

void Task::addEvent(const TaskEvent& event)
{
    if (event.getType() != TaskEvent::UNKNOWN) {
        m_eventList[event.getType() - 1].push_back(event);
    }
}

void Task::addEvents(const TaskEventList& eventList)
{
    for (int i = 0; i < eventList.size(); i++) {
        addEvent(eventList.at(i));
    }
}

TaskEvent Task::getFirstEvent(const TaskEvent::TaskEventType& type) const
{
    TaskEvent ret;

    if (type != TaskEvent::UNKNOWN) {
        ret = m_eventList[type - 1].front();
    }

    return ret;
}

TaskEvent Task::getLastEvent(const TaskEvent::TaskEventType& type) const
{
    TaskEvent ret;

    if (type != TaskEvent::UNKNOWN) {
        ret = m_eventList[type - 1].back();
    }

    return ret;
}

int Task::getNumEvents(const TaskEvent::TaskEventType& type) const
{
    int ret = 0;

    if (type != TaskEvent::UNKNOWN) {
        ret = m_eventList[type - 1].size();
    }

    return ret;
}

Task::TaskEventList Task::getEvents(const TaskEvent::TaskEventType& type) const
{
    TaskEventList ret;

    if (type != TaskEvent::UNKNOWN) {
        ret = m_eventList[type - 1];
    }

    return ret;
}

int Task::getNumEvents() const
{
    int ret = 0;

    int a;
    for (a = TaskEvent::CREATED; a <= TaskEvent::SCHEDULED; a++) {
        ret += getNumEvents(TaskEvent::TaskEventType(a));
    }

    return ret;
}

Task::TaskEventList Task::getEvents() const
{
    TaskEventList ret;

    int a;
    for (a = TaskEvent::CREATED; a <= TaskEvent::SCHEDULED; a++) {
        ret.append(getEvents(TaskEvent::TaskEventType(a)));
    }

    return ret;
}


//const int TaskEvent::MAX_EVENTS;

TaskEvent::TaskEvent(
    const int& userid,
    const TaskEventType& type)
{
    m_userid = userid;
    m_type = type;
}

TaskEvent::TaskEvent()
{
    m_userid = USERDATA_ID_INVALID;
    m_type = UNKNOWN;
}

void TaskEvent::setUser(const int& userid)
{
    m_userid = userid;
}

void TaskEvent::setType(const TaskEventType& type)
{
    m_type = type;
}

void TaskEvent::setTime(const QDateTime& time)
{
    m_timestamp = time;
}

void TaskEvent::setTime()
{
    setTime(QDateTime::currentDateTime());
}

TaskEvent& TaskEvent::operator=(const TaskEvent& rhs)
{
    if (this != &rhs) {
        m_userid = rhs.m_userid;
        m_type = rhs.m_type;
        m_timestamp = rhs.m_timestamp;
    }

    return *this;
}

QString TaskEvent::toTypeString(const TaskEvent::TaskEventType& type)
{
    QString ret("Unknown");
    switch (type) {
    case CREATED: ret = "Created"; break;
    case REMOVED: ret = "Removed"; break;
    case COMPLETED: ret = "Completed"; break;
    case STARTED: ret = "Started"; break;
    case STOPPED: ret = "Stopped"; break;
    case SCHEDULED: ret = "Scheduled"; break;
    }

    return ret;
}
