#include "tasklist.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFrame>
#include <QLabel>
#include <QToolButton>
#include <QDebug>

TaskList::TaskList(QWidget *parent) :
    QWidget(parent)
{
    QVBoxLayout *vbox = new QVBoxLayout;
    QHBoxLayout *search = new QHBoxLayout;
    QFrame *ml = new QFrame;

    QLabel *show = new QLabel;
    QLabel *s = new QLabel;

    m_listWidget = new QListWidget;

    m_DateEdit = new QDateTimeEdit(QDate::currentDate());

    ml->setObjectName("searchframe");
    ml->setStyleSheet("#searchframe {"
        "border: 1px solid black;"
        "background-color: #ffff80; }");
    //ml->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    ml->setMinimumHeight(32);

    show->setText("&nbsp;&nbsp;<b>Showing Tasks for</b> ");
    show->setStyleSheet("QLabel { border: 0px; }");

    s->setText("&nbsp;&nbsp;<b>Search</b> ");
    s->setStyleSheet("QLabel { border: 0px; }");

    m_DateEdit->setMinimumDate(QDate::currentDate().addDays(-365));
    m_DateEdit->setMaximumDate(QDate::currentDate().addDays(365));
    m_DateEdit->setDisplayFormat("dddd, MMMM d, yyyy");
    m_DateEdit->setCalendarPopup(true);

    m_SearchBar = new QLineEdit;

    QToolButton *go = new QToolButton;
    go->setAutoRaise(true);
    go->setText(tr("Search"));
    go->setIcon(QIcon(":/images/magnifier.png"));

    connect(go, SIGNAL(clicked()), this, SLOT(search()));
    connect(m_DateEdit, SIGNAL(dateTimeChanged(QDateTime)), this, SLOT(updateResults(QDateTime)));

//    m_TaskList = new TaskListView;

//    TaskListModel *model = new TaskListModel;

//    model->addTask(t);
//    model->addTask(u);

//    TaskListDelegate *d = new TaskListDelegate(m_TaskList);
//    m_TaskList->setModel(model);
//    m_TaskList->setItemDelegate(d);

//    QListWidgetItem *item = new QListWidgetItem;
//    m_TaskList->addItem(item);
//    m_TaskList->setItemWidget(item, new TaskListTasklet(t));

//    m_TaskList->setEditTriggers(QAbstractItemView::AllEditTriggers);

    search->setContentsMargins(2, 2, 2, 0);
//    search->addWidget(show);
//    search->addWidget(m_DateEdit);
    search->addWidget(s);
    search->addWidget(m_SearchBar);
    search->addWidget(go);

    m_View = new TaskListView;
//    m_View->setRenderHint(QPainter::Antialiasing, false);
//    m_View->setDragMode(QGraphicsView::RubberBandDrag);
//    m_View->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    m_View->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);

    m_Scene = new TaskListScene;
    m_Scene->setSceneRect(0, 0, 450, 60);

    m_View->setAlignment(Qt::AlignTop | Qt::AlignHCenter);
    m_View->setScene(m_Scene);
    m_View->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

    ml->setLayout(search);

    connect(m_listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClicked(QListWidgetItem*)));
    connect(m_listWidget, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(itemChanged(QListWidgetItem*)));

    connect(m_Scene, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

    vbox->setContentsMargins(0, 0, 0, 0);
    vbox->setSpacing(1);
    vbox->addWidget(ml);
//    vbox->addWidget(m_TaskList);
//    vbox->addWidget(m_listWidget);
    vbox->addWidget(m_View);

    setMouseTracking(true);
    setLayout(vbox);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
}

void TaskList::updateScene()
{
    m_Scene->updateScene();
}

void TaskList::search()
{

}

void TaskList::mouseMoveEvent(QMouseEvent *event)
{
    if (!m_Scene->sceneRect().contains(event->pos())) {
        m_Scene->updateScene();
    }

    QWidget::mouseMoveEvent(event);
}

void TaskList::updateResults(const QDateTime& newdate)
{
    ((void)newdate);
}

void TaskList::addTask(const int& taskid)
{
//    m_TaskList->addTask(task);

    TaskListItem *item = new TaskListItem(taskid);
    item->setPos(5, (60 * m_listItems.size()) + 5);

    m_listItems.push_back(item);

    m_Scene->add(item);
}

void TaskList::itemClicked(QListWidgetItem* item)
{
    Q_UNUSED(item);
}

void TaskList::itemChanged(QListWidgetItem* item)
{
    Q_UNUSED(item);
}

void TaskList::selectionChanged()
{
    QList<QGraphicsItem*> items = m_Scene->selectedItems();
    int taskid = INVALID_TASK;

    if (items.size() == 0) {
        // nothing selected

        emit taskChanged(INVALID_TASK);
    } else if (items.size() == 1) {
        // signal selected
        TaskListItem *p = dynamic_cast<TaskListItem*>(items.first());

        if (p != NULL) {
            taskid = p->getId();

//            qDebug() << "signal taskChanged(" << taskid << ")";
        }

        emit taskChanged(taskid);
    } else {
        // signal multiple?
    }
}
