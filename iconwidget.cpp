#include "iconwidget.h"
#include <QPainter>
#include <QPixmap>

IconWidget::IconWidget(QWidget *parent) :
    QWidget(parent)
{
    setMouseTracking(true);
}

IconWidget::IconWidget(const QIcon& icon, QWidget *parent) :
    QWidget(parent)
{
    m_Icon = icon;
    setMouseTracking(true);
}

void IconWidget::setWidgetIcon(const QIcon& icon)
{
    m_Icon = icon;

    repaint();
}

QIcon IconWidget::getWidgetIcon() const
{
    return m_Icon;
}

void IconWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    QPixmap pixmap = m_Icon.pixmap(size(), QIcon::Normal, QIcon::On);

    painter.drawPixmap(0, 0, pixmap);

    Q_UNUSED(event);
}
