#ifndef TASKLOGINSCREEN_H
#define TASKLOGINSCREEN_H

#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QIcon>
#include <QToolButton>
#include <QCheckBox>

#include "taskiconlabel.h"
#include "global.h"
#include "iconwidget.h"

class TaskLoginScreen : public QLabel, public GlobalState
{
    Q_OBJECT
public:
    explicit TaskLoginScreen(GlobalListener *listener, QWidget *parent = 0);

    void setButtonEnabled(bool enabled);

    void showEvent(QShowEvent *event);

    void keyPressEvent(QKeyEvent *ev);
    void mousePressEvent(QMouseEvent *ev);

    void beginState();
    void endState();
private:
    QLabel *m_directions;
    QLabel *m_labelUsername;
    QLabel *m_labelPassword;
    IconWidget *m_labelIcon;
    QCheckBox *m_rememberMe;

    QLineEdit *m_username;
    QLineEdit *m_password;

    QPushButton *m_loginButton;
    QPushButton *m_quitButton;
    GlobalListener *m_listener;
    TaskIconLabel *m_labelInfo;

signals:
    void login(const QString& username, const QString& password);

public slots:
    void loginClicked();
};

#endif // TASKLOGINSCREEN_H
