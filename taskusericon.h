#ifndef TASKUSERICON_H
#define TASKUSERICON_H

#include <QLabel>

class TaskUserIcon : public QLabel
{
    Q_OBJECT
public:
    explicit TaskUserIcon(QWidget *parent = 0);

    void setDragTarget();

signals:

public slots:

private:
    QLabel *m_Picture;
    QLabel *m_Name;
};

#endif // TASKUSERICON_H
