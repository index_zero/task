#include "desktopwidget.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QIcon>
#include <QSpacerItem>
#include <QFont>
#include <QPixmap>

#include "taskobjectproperties.h"

DesktopWidget::DesktopWidget(QWidget *parent) :
    QFrame(parent)
{
    QVBoxLayout *welcome = new QVBoxLayout;
    QSpacerItem *spacer = new QSpacerItem(64, 64, QSizePolicy::Expanding, QSizePolicy::Expanding);

    QLabel *w = new QLabel;
    QLabel *msg = new QLabel;

    QIcon icon(":/images/welcome.png");
    QPixmap pixmap = icon.pixmap(QSize(360, 130), QIcon::Normal, QIcon::On);

    w->setPixmap(pixmap);

    QFont font("Verdana", 10);

    QWidget *a = new QWidget;
    TaskObjectProperties *prop = new TaskObjectProperties(a);
    prop->setTitle("<b>Task Statistics</b>");

    msg->setFont(font);
    msg->setText("Task lets you organize workflow and manage projects in a simple, yet surprisingly power way.\n\n\n");

    welcome->setSpacing(0);
    welcome->setMargin(6);
    welcome->addWidget(w);
    welcome->addWidget(msg);
    welcome->addWidget(prop);
    welcome->addSpacerItem(spacer);
    welcome->setAlignment(w, Qt::AlignLeft | Qt::AlignTop);
    welcome->setAlignment(msg, Qt::AlignLeft | Qt::AlignTop);

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    setStyleSheet("background-color: #ffffff;");
    setLayout(welcome);
}
