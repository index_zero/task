#ifndef TASKLISTVIEW_H
#define TASKLISTVIEW_H

#include <QGraphicsView>
#include <QScrollBar>

#include "task.h"
#include "tasklistscene.h"

#include <list>
using namespace std;

class TaskListView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit TaskListView(QWidget *parent = 0);

    void resizeEvent(QResizeEvent *event);

protected:
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

private:

signals:

public slots:

};

#endif // TASKLISTVIEW_H
