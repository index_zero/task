#ifndef TASK_H
#define TASK_H

#include <QString>
#include <QIcon>
#include <QDateTime>

enum Weekday
{
    wSUNDAY = 0,
    wMONDAY,
    wTUESDAY,
    wWEDNESDAY,
    wTHURSDAY,
    wFRIDAY,
    wSATURDAY
};

enum RepeatType
{
    rtDAILY,
    rtWEEKLY,
    rtMONTHLY,
    rtYEARLY,
    rtWEEKDAY,      // monday-friday
    rtMONWEDFRI,    // monday, wednesday, friday
    rtTUETHUR       // tuesday, thursday
};

enum TaskSeverity
{
    tsMINOR,
    tsAVERAGE,
    tsMAJOR,
    tsCRITICAL
};

enum TaskPriority
{
    tsLOW,
    tsROUTINE,
    tsURGENT,
    tsIMMEDIATE
};

enum TaskState
{
    tsNOTSTARTED,
    tsSTARTED,
    tsFINISHED
};

typedef QList<QDate> TaskDateList;
typedef QList<Weekday> TaskWeekdayList;

class TaskRecurrence
{
public:
protected:
    bool m_excludeHolidays; // true if holidays are excluded, even if the task would
                            // fall on this day
    TaskDateList m_excludeDays;

    TaskWeekdayList m_repeatDays;   // list of days on which the task repeats

    RepeatType m_repeatType;
    int m_repeatFrequency;          // for rtDAILY, rtWEEKLY, rtMONTHLY, or rtYEARLY this
                                    // variable defines how often to repeat (for example,
                                    // with rtWEEKLY, the task repeates every m_repeatFrequency
                                    // weeks).

    QDate m_startDate;

    QDate m_endDate;        // end date
    int m_numOccurrences;   // # of occurrences (if this is non-zero it overrides end date)

    bool m_neverEnding;     // true if never ending

};

#define INVALID_TASK (-1)

class TaskEvent
{
public:
    enum TaskEventType
    {
        UNKNOWN = 0,
        CREATED,
        REMOVED,
        COMPLETED,
        STARTED,
        STOPPED,
        SCHEDULED
    };

    static const int MAX_EVENTS = 6;

public:
    explicit TaskEvent(
        const int& userid,
        const TaskEventType& type);

    explicit TaskEvent();

    virtual ~TaskEvent() { }

    void setUser(const int& userid);
    void setType(const TaskEventType& type);
    void setTime(const QDateTime& time);
    // sets current time
    void setTime();

    int getUser() const { return m_userid; }
    TaskEventType getType() const { return m_type; }
    QDateTime getTime() const { return m_timestamp; }

    TaskEvent& operator=(const TaskEvent& rhs);

    static QString toTypeString(const TaskEvent::TaskEventType& type);
protected:

private:
    TaskEventType m_type;
    int m_userid;
    QDateTime m_timestamp;

};

class Task
{
public:
    typedef QList<TaskEvent> TaskEventList;

public:
    explicit Task(const int& id,
                  const int& owner,
                  const QString& title,
                  const QString& desc);

    explicit Task(const int& id);

    virtual ~Task() { }

    QString getTitle() const { return m_title; }
    QString getDescription() const { return m_description; }

    void setColor(const QColor& color);
    void setTitle(const QString& title);
    void setDescription(const QString& desc);
    void setOwner(const int owner);

    QColor getColor() const { return m_color; }
    QDateTime getLastModified() const { return m_lastModified; }
    int getOwner() const { return m_owner; }

    bool isCompleted() const;

    int getId() const { return m_id; }

    Task& operator=(const Task& t);

    void addEvent(const TaskEvent& event);
    void addEvents(const TaskEventList& eventList);

    TaskEvent getFirstEvent(const TaskEvent::TaskEventType& type) const;
    TaskEvent getLastEvent(const TaskEvent::TaskEventType& type) const;

    int getNumEvents(const TaskEvent::TaskEventType& type) const;
    TaskEventList getEvents(const TaskEvent::TaskEventType& type) const;

    int getNumEvents() const;
    TaskEventList getEvents() const;
protected:
    void setDefaults();

private:
    QDateTime m_lastModified;

    int m_id;
    QColor m_color;

    int m_owner;

    QString m_title;
    QString m_description;
    TaskEventList m_eventList[TaskEvent::MAX_EVENTS];
};

class ScheduledTask : public Task
{
public:
protected:
    QDateTime m_startTime;
    QDateTime m_endTime;
    bool m_daily;           // true if this event lasts all day
    bool m_alert;           // true if an alert (only start time is relavent)

    TaskRecurrence m_recurrence;
};

class UnscheduledTask : public Task
{
public:
    void setPriority(const TaskPriority& p);
    void setSeverity(const TaskSeverity& s);
    void setState(const TaskState& s);

    TaskPriority getPriority() const;
    TaskSeverity getSeverity() const;
    TaskState getState() const;

protected:
    TaskPriority m_priority;	// priority
    TaskSeverity m_severity;	// severity
    TaskState m_state;      	// state
};


#endif // TASK_H
