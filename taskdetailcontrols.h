#ifndef TASKDETAILCONTROLS_H
#define TASKDETAILCONTROLS_H

#include <QComboBox>
#include <QTextEdit>
#include <QPlainTextEdit>
#include <QLineEdit>
#include <QDateTimeEdit>
#include <QHeaderView>
#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>

// this file contains several control overrides which have special
// style sheets designed to be displayed on the taskdetailpanel

// combo box
class TaskDetailComboBox : public QComboBox
{
    Q_OBJECT
public:
    TaskDetailComboBox(QWidget *parent = 0);
};

// line edit
class TaskDetailLineEdit : public QLineEdit
{
    Q_OBJECT
public:
    TaskDetailLineEdit(QWidget *parent = 0);
};

// plain text edit
class TaskDetailPlainTextEdit : public QPlainTextEdit
{
    Q_OBJECT
public:
    TaskDetailPlainTextEdit(QWidget *parent = 0);
};

// date time edit
class TaskDetailDateTimeEdit : public QDateTimeEdit
{
    Q_OBJECT
public:
    TaskDetailDateTimeEdit(QWidget *parent = 0);
};

class TaskDetailHeaderView : public QHeaderView
{
    Q_OBJECT
public:
    TaskDetailHeaderView(QWidget *parent = 0);
};

class TaskDetailGroupBox : public QGroupBox
{
    Q_OBJECT
public:
    TaskDetailGroupBox(QWidget *parent = 0);
};

class TaskDetailGroupWidget : public QVBoxLayout
{
    Q_OBJECT
public:
    TaskDetailGroupWidget();

    void setInnerLayout(QLayout *layout);

private:
    QWidget *m_widget;
    QVBoxLayout *m_layout;

public slots:
    void setVisible(bool visible);
};

class TaskDetailLabel : public QLabel
{
    Q_OBJECT
public:
    TaskDetailLabel(QWidget *parent = 0);
    TaskDetailLabel(const QString& text, QWidget *parent = 0);

public slots:
    void setModified(bool modified);

signals:
    void labelModified(bool modified);

protected:
    void initializeStyleSheets();

private:
    bool m_modified;

    QString m_modifiedStyleSheet;
    QString m_normalStyleSheet;
};

#endif // TASKDETAILCONTROLS_H
