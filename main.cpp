#include <QApplication>
#include <QStyleFactory>
#include "taskapp.h"
#include "mainwindow.h"
#include "global.h"
#include "taskglobal.h"

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    TaskApp app(argc, argv);
    TaskGlobal::create(&app);
    int rc;

    app.setOrganizationName("http://www.theindexzero.com");
    app.setOrganizationDomain("http://www.theindexzero.com");
    app.setApplicationName("Tasks");

    app.setStyle(QStyleFactory::create("Fusion"));

    TaskMainWindow wnd;

    app.setMainWindow(&wnd);

    // do any systems have resolution lower than this?
    wnd.setMinimumSize(1024, 768);
    //wnd.showNormal();
    wnd.show();
    //wnd.setWindowState(Qt::WindowMaximized);

    rc = app.exec();

    TaskGlobal::destroy();

    return rc;
}
