#include "taskglobal.h"

#include <QCryptographicHash>
#include <QMessageBox>

static QByteArray byteToHex(const QByteArray& bytes)
{
    const char hexed[16] = {
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };
    QByteArray ret;
    int i;

    for (i = 0; i < bytes.size(); i++) {
        unsigned char z = (unsigned char)bytes.at(i);
        ret.append(hexed[(z & 0xf0) >> 4]);
        ret.append(hexed[z & 0x0f]);
    }

    return ret;
}


TaskGlobal *TaskGlobal::m_instance = static_cast<TaskGlobal*>(0);

TaskGlobal::TaskGlobal(TaskApp *app) : m_app(app)
{
    m_networkObject = NULL;

    for (int i = 1; i <= MAX_TASKS; i++) {
        m_taskIdList.insert(i);
    }
}

TaskGlobal::~TaskGlobal(void)
{
}

void TaskGlobal::destroy()
{
    if (m_instance->m_networkObject != NULL) {
       delete m_instance->m_networkObject;
    }

    if (m_instance != static_cast<TaskGlobal*>(0)) {
        delete m_instance;
    }
}

void TaskGlobal::create(TaskApp *app)
{
    if (m_instance == static_cast<TaskGlobal*>(0)) {
        m_instance = new TaskGlobal(app);

        m_instance->initialize();
    }
}

void TaskGlobal::initialize()
{
    m_networkObject = new GlobalNetworkObject();
}

void TaskGlobal::login(
    GlobalListener *listener,
    const QString username,
    const QString password)
{
    QByteArray hpass = QCryptographicHash::hash(password.toLocal8Bit(), QCryptographicHash::Sha1);

    //"u=steve&p=8c23ff5929439f3675b2297a7541fe4b8e3892f0"

    QByteArray q;

    q.append("f=login&u=");
    q.append(username);
    q.append("&p=");
    q.append(byteToHex(hpass));
//    q.append("\n");

    m_instance->m_networkObject->query(listener, q, qLOGIN);
}

void TaskGlobal::loadUserData(GlobalListener *listener, const int id)
{
    QString sz;
    QByteArray q;

    sz.sprintf("f=udata&id=%d", id);
    q.append(sz);

    m_instance->m_networkObject->query(listener, q, qUSERDATA);
}

void TaskGlobal::setUserData(const DataUser& data)
{
   m_instance->m_user = data;
}

DataUser TaskGlobal::getUserData()
{
    return m_instance->m_user;
}

void TaskGlobal::loadTask(const Task& t)
{
    if (m_instance->m_taskIdList.contains(t.getId())) {
        m_instance->m_taskIdList.remove(t.getId());

        Task *task = new Task(t.getId());
        task->setDescription(t.getDescription());
        task->setColor(t.getColor());
        task->setOwner(t.getOwner());
        task->setTitle(t.getTitle());

        // add events
        task->addEvents(t.getEvents(TaskEvent::CREATED));
        task->addEvents(t.getEvents(TaskEvent::COMPLETED));
        task->addEvents(t.getEvents(TaskEvent::REMOVED));
        task->addEvents(t.getEvents(TaskEvent::STOPPED));
        task->addEvents(t.getEvents(TaskEvent::STARTED));
        task->addEvents(t.getEvents(TaskEvent::SCHEDULED));

        m_instance->m_taskMap.insert(t.getId(), task);
    }
}

Task* TaskGlobal::newTask()
{
    Task *ret = NULL;
    int id;
    QSet<int>::iterator it;

    if (m_instance->m_taskIdList.size() > 0) {
        it = m_instance->m_taskIdList.begin();

        id = *it;
        m_instance->m_taskIdList.erase(it);

        ret = new Task(id);

        m_instance->m_taskMap.insert(id, ret);
    }

    return ret;
}

Task* TaskGlobal::getTask(const int& id)
{
    Task *ret = NULL;
    QMap<int, Task*>::iterator it =
            m_instance->m_taskMap.find(id);

    if (it != m_instance->m_taskMap.end()) {
        ret = it.value();
    }

    return ret;
}

QAction* TaskGlobal::getAction(const QString& name)
{
    QAction *ret = NULL;
    ActionMap::Iterator it = m_instance->m_actionMap.find(name);

    if (it != m_instance->m_actionMap.end()) {
        ret = it.value();
    }

    return ret;
}

void TaskGlobal::setAction(const QString& name, QAction *action)
{
    m_instance->m_actionMap.insert(name, action);
}

void TaskGlobal::loadWeather(GlobalListener *listener, const QString& url)
{
    QByteArray q;

    m_instance->m_networkObject->query(listener, q, url, qWEATHER);
}

void TaskGlobal::rawTaskQuery(
    GlobalListener *listener,
    const PostDataMap& postData,
    const QueryFunction& func)
{
    QStringList sz;
    QByteArray q;

    switch (func) {
    case qLOGIN: sz.push_back("f=login"); break;
    case qCNT: sz.push_back("f=cnt"); break;
    case qUSERDATA: sz.push_back("f=udata"); break;
    case qUSERS: sz.push_back("f=users"); break;
    case qTASKDATA: sz.push_back("f=tdata"); break;
    case qTASKS: sz.push_back("f=tasks"); break;
    case qLOGOUT: sz.push_back("f=logout"); break;
    default: break;
    }

    PostDataMap::const_iterator it;

    for (it = postData.begin(); it != postData.end(); it++) {
        QString s;
        s.append(it.key());
        s.append("=");
        s.append(GlobalNetworkObject::encode(it.value()));
        sz.push_back(s);
    }

    if (sz.size() > 0) {
        q.append(sz.join("&"));
    }

    qDebug() << q;

    m_instance->m_networkObject->query(listener, q, func);
}

void TaskGlobal::addUser(const DataUser& user)
{
    m_instance->m_userMap.insert(user.getId(), user);
}

DataUser TaskGlobal::getUser(const int& id)
{
    DataUser ret;
    QMap<int, DataUser>::Iterator it = m_instance->m_userMap.find(id);

    if (it != m_instance->m_userMap.end()) {
        ret = it.value();
    }

    return ret;
}

void TaskGlobal::setSessionId(const QString& sess)
{
    m_instance->m_sessionId.clear();;
    m_instance->m_sessionId.append(sess);
}

TaskGlobal::TaskList TaskGlobal::getTasks()
{
    return m_instance->m_taskMap.values();
}


TaskGlobal::UserList TaskGlobal::getUsers()
{
    return m_instance->m_userMap.values();
}
