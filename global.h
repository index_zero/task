#ifndef GLOBAL_H
#define GLOBAL_H

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QBuffer>
#include <QXmlStreamReader>
#include <QUrl>
#include <QDebug>
#include <QMutex>

#include "datauser.h"

class GlobalListener
{
public:
    typedef void (*handleFunc)(QXmlStreamReader*);

public:
    GlobalListener(void) { }
    virtual ~GlobalListener(void) { }

    virtual void handleLogin(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleUserData(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleWeatherData(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleCounts(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleUsers(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleTaskData(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleTasks(QXmlStreamReader *xml) { Q_UNUSED(xml); }
    virtual void handleLinks(QXmlStreamReader *xml) { Q_UNUSED(xml); }
};

enum QueryFunction
{
    qFIRST = 0,

    qNONE = qFIRST,
    qLOGIN,         // login
    qLOGOUT,        // logout
    qCNT,           // counts size of tables
    qUSERDATA,      // id=?
    qUSERS,         // all users
    qTASKDATA,      // id=?
    qTASKS,         // all tasks
    qWEATHER,       // weather

    qLINKS,

    qLAST,
    qCOUNT = (qLAST - qFIRST)
};

class GlobalNetworkObject : public QObject
{
    Q_OBJECT
public:
    explicit GlobalNetworkObject(QObject * parent = 0);
    virtual ~GlobalNetworkObject();

    QNetworkAccessManager* getNetworkManager();

    void query(GlobalListener *listener, QByteArray q, const QueryFunction& f);
    void query(GlobalListener *listener, QByteArray q, const QString& url, const QueryFunction& f);

    // RFC 3986 rawurlencode()
    static QByteArray encode(const QString& data);
protected:
    void getUrl(const QUrl& url);

    QNetworkAccessManager *m_manager;
    QNetworkReply *m_currentReply;
    QByteArray m_postData;
private:
    QXmlStreamReader m_xml;
    QueryFunction m_lastFunction;
    GlobalListener *m_listener;
    bool m_insideQuery;

    QByteArray m_replyData;
    QMutex m_mutex;

signals:

public slots:
    void finished(QNetworkReply *reply);

    void readReady();
    void readFinished();
    void metaDataChanged();
    void error(QNetworkReply::NetworkError err);
};

class GlobalState
{
public:
    GlobalState(void) { }
    virtual ~GlobalState(void) { }

    virtual void beginState() { }
    virtual void endState() { }
};

#endif // GLOBAL_H
