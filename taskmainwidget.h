#ifndef TASKMAINWIDGET_H
#define TASKMAINWIDGET_H

#include <QWidget>
#include <QMenuBar>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QButtonGroup>
#include <QToolBar>
#include <QToolButton>
#include <QStackedLayout>
#include <QSplitter>

#include "global.h"
#include "tasklist.h"
#include "taskdetailpanel.h"

class TaskMainWidget : public QWidget, public GlobalListener, public GlobalState
{
    Q_OBJECT
public:
    enum SecondaryViews {
        svTASK_DETAIL = 0,
        svITINERARY,
        svCALENDAR
    };

    explicit TaskMainWidget(QWidget *parent = 0);

    TaskList *getTaskList() { return m_TaskList; }
    void setupTaskList();

protected:
    void setupViewPane();
    void setupSecondaryView();

    void mouseMoveEvent(QMouseEvent *event);

private:
    // Splitter
    QSplitter *m_splitter;

    // Task list
    TaskList *m_TaskList;

    // View Pane Toolbar
    QButtonGroup *m_viewGroup;
    QToolBar *m_viewPage;

    QToolButton *m_taskDetailBtn;
    QToolButton *m_itineraryBtn;
    QToolButton *m_calendarBtn;
    QToolButton *m_addTaskBtn;

    // Toolbar (each secondary view can have it's own toolbar)
    QToolBar *m_mainToolBar;
    TaskDetailPanel *m_panelDetail;

    // Secondary views
    QStackedLayout *m_secondaryLayout;

signals:

public slots:

};

#endif // TASKMAINWIDGET_H
