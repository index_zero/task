#ifndef TASKAPP_H
#define TASKAPP_H

#include <QApplication>
#include "mainwindow.h"

class TaskApp : public QApplication
{
    Q_OBJECT
public:
    TaskApp(int &argc, char **argv);


    TaskMainWindow *MainWindow();
    void setMainWindow(TaskMainWindow *wnd);
signals:

public slots:

private:
    TaskMainWindow *m_MainWnd;
};

#endif // TASKAPP_H
