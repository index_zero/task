#include "tasklistscene.h"

#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include "tasklistitem.h"

TaskListScene::TaskListScene(QObject *parent) :
    QGraphicsScene(parent)
{
    m_lastItem = NULL;
    m_trashBtnProxy = NULL;

    m_trashBtn = new QPushButton;
    m_trashBtn->setIconSize(QSize(16, 16));
    m_trashBtn->setFlat(true);
    m_trashBtn->setToolTip("Move to Trash");
    m_trashBtn->setObjectName("trashBtn");
    m_trashBtn->setStyleSheet("#trashBtn { background-color: rgba(255, 255, 255, 0% ); }");
    m_trashBtn->setIcon(QIcon(":/images/small/bin.png"));
    m_trashBtn->setCursor(QCursor(Qt::PointingHandCursor));

    m_trashBtn->setVisible(false);
    m_trashBtnProxy = addWidget(m_trashBtn);

    //setMouseTracking(true);
    //connect(this, SIGNAL(sceneRectChanged(QRectF)), this, SLOT(onSceneRectChanges(QRectF)));

    redraw();
}

void TaskListScene::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem* pItemUnderMouse = itemAt(event->scenePos().x(), event->scenePos().y(), QTransform());

    if (pItemUnderMouse != m_trashBtnProxy) {
        clearSelection();
    }

    QGraphicsScene::mousePressEvent(event);

    invalidate();
}

void TaskListScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    m_trashBtn->setDown(false);

    QGraphicsScene::mouseReleaseEvent(event);
}

void TaskListScene::keyPressEvent(QKeyEvent *event)
{
    int i;

    if (event->key() == Qt::Key_Down) {
        if (selectedItems().size() > 0) {
            // get the last selected item
            i = m_itemIndex.value(selectedItems().last());

            // move the selection to the next item if possible
            if (i < (m_itemList.size() - 1)) { i++; }

            // select that item
            clearSelection();
            m_itemList.at(i)->setSelected(true);
            m_itemList.at(i)->ensureVisible(QRectF(), 5, 5);

            event->accept();
        } else {
            QGraphicsScene::keyPressEvent(event);
        }
    } else if (event->key() == Qt::Key_Up) {
        if (selectedItems().size() > 0) {
            // get the last selected item
            i = m_itemIndex.value(selectedItems().last());

            // move the selection to the next item if possible
            if (i > 0) { i--; }

            // select that item
            clearSelection();
            m_itemList.at(i)->setSelected(true);
            m_itemList.at(i)->ensureVisible(QRectF(), 5, 5);

            event->accept();
        } else {
            QGraphicsScene::keyPressEvent(event);
        }
    } else {
        QGraphicsScene::keyPressEvent(event);
    }
}

void TaskListScene::keyReleaseEvent(QKeyEvent *event)
{
    QGraphicsScene::keyReleaseEvent(event);
}

void TaskListScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem* pItemUnderMouse = itemAt(event->scenePos().x(), event->scenePos().y(), QTransform());

    if ((pItemUnderMouse != m_lastItem) &&
        (pItemUnderMouse != m_trashBtnProxy)) {
        // reset old highlight
        TaskListItem *t = dynamic_cast<TaskListItem *>(m_lastItem);
        if (t) {
            t->setHighlight(false);
        }

        m_lastItem = pItemUnderMouse;

        // clear old drawing
        m_trashBtn->setVisible(false);

        // draw on m_lastItem
        if (m_lastItem) {
            int x, y;

            m_trashBtn->setVisible(true);

            x = m_lastItem->boundingRect().x() +
                (m_lastItem->boundingRect().width() -
                 m_trashBtnProxy->size().width());
            y = m_lastItem->pos().y() + SIZE_BORDER;

            m_trashBtnProxy->setPos(x, y);

            invalidate();
        }
    }

    if (pItemUnderMouse == m_trashBtnProxy) {
        TaskListItem *t = dynamic_cast<TaskListItem *>(m_lastItem);

        if (t) {
            t->setHighlight(true);
        }
    }

    if (pItemUnderMouse == NULL) {
        TaskListItem *t = dynamic_cast<TaskListItem *>(m_lastItem);
        if (t) {
            t->setHighlight(false);
            m_trashBtn->setVisible(false);
            m_lastItem = NULL;
        }
    }

//    printf("TaskListScene::mouseMoveEvent: %.4fx%.4f\n", event->scenePos().x(), event->scenePos().y());

    // this properly notifies the child for redraw (for hover events)
    QGraphicsScene::mouseMoveEvent(event);
}

void TaskListScene::add(QGraphicsItem *item)
{
    if (!m_itemList.contains(item)) {
        item->setPos(5, (60 * m_itemList.size()) + 5);
        m_itemIndex.insert(item, m_itemList.size());
        m_itemList.push_back(item);
    }

    redraw();
}

void TaskListScene::remove(QGraphicsItem *item)
{
    for (int i = 0; i < m_itemList.size(); i++) {
        if (m_itemList.at(i) == item) {
            m_itemList.removeAt(i);
            m_itemIndex.erase(m_itemIndex.find(item));
            removeItem(item);
            break;
        }
    }
}

void TaskListScene::removeAll()
{
    removeAllItems();

    m_itemIndex.clear();
    m_itemList.clear();
}

void TaskListScene::updateScene()
{
    TaskListItem *t = dynamic_cast<TaskListItem *>(m_lastItem);

    if (t) {
        t->setHighlight(false);
        m_trashBtn->setVisible(false);
        m_lastItem = NULL;
    }

    invalidate();
}

void TaskListScene::redraw()
{
    removeAllItems();

    for (int i = 0; i < m_itemList.size(); i++) {
        QGraphicsItem *item = m_itemList.at(i);
        item->setPos(5, (60 * i) + 5);

        addItem(item);
    }

    addItem(m_trashBtnProxy);
}

void TaskListScene::removeAllItems()
{
    while (items().size() > 0) {
        removeItem(items().back());
    }
}

void TaskListScene::updateRect(const QRectF& rect)
{
    QRectF old = rect;

    old.setHeight((60 * m_itemList.size()) + 10);

    for (int i = 0; i < m_itemList.size(); i++) {
        TaskListItem *t = dynamic_cast<TaskListItem *>(m_itemList.at(i));
        if (t) {
            t->setItemWidth(old.width() - 10);
        }
    }

    setSceneRect(old);

    invalidate();
}

int TaskListScene::getItemIndex(QGraphicsItem *item)
{
    QMap<QGraphicsItem*, int>::Iterator it = m_itemIndex.find(item);
    int id = INVALID_TASK;

    if (it != m_itemIndex.end()) {
        id = it.value();
    }

    return id;
}

//void TaskListScene::onSceneRectChanges(const QRectF& rect)
//{
//    for (int i = 0; i < m_itemList.size(); i++) {
//        TaskListItem *t = dynamic_cast<TaskListItem *>(m_itemList.at(i));
//        if (t) {
//            t->setItemWidth(rect.width() - 10);
//        }
//    }

//    redraw();
//}
