#ifndef TASKGLOBAL_H
#define TASKGLOBAL_H

#include "global.h"
#include "taskapp.h"
#include "datauser.h"
#include <QSet>
#include <QMap>
#include <QAction>

#define MAX_TASKS   65536

class TaskGlobal
{
public:
    typedef QMap<QString, QByteArray> PostDataMap;
    typedef QList<Task*> TaskList;
    typedef QList<DataUser> UserList;
    typedef QMap<QString, QAction *> ActionMap;

public:
    static void create(TaskApp *app);

    static TaskGlobal* getInstance() { return m_instance; }

    static void destroy();

    static TaskApp *getApp() { return m_instance->m_app; }

    static QNetworkAccessManager* getNetworkManager();

    static void login(
        GlobalListener *listener,
        const QString username,
        const QString password);

    static void setUserData(const DataUser& data);
    static DataUser getUserData();

    // these function load off the internet
    static void loadUserData(GlobalListener *listener, const int id);
    static void loadWeather(GlobalListener *listener, const QString& url);

    static void rawTaskQuery(
        GlobalListener *listener,
        const PostDataMap& postData,
        const QueryFunction& func);

    static void loadTask(const Task& t);
    static Task* newTask();
    static Task* getTask(const int& id);
    static int getNumTasks() { return m_instance->m_taskMap.size(); }
    static TaskList getTasks();

    static void addUser(const DataUser& user);
    static DataUser getUser(const int& id);
    static int getNumUsers() { return m_instance->m_userMap.size(); }
    static UserList getUsers();

    static QAction *getAction(const QString& name);
    static void setAction(const QString& name, QAction *action);

    static void setSessionId(const QString& sess);
    static QByteArray getSessionId() { return m_instance->m_sessionId; }
protected:
    explicit TaskGlobal(TaskApp *app);
    virtual ~TaskGlobal(void);

    void initialize();

    static TaskGlobal *m_instance;

    QSet<int> m_taskIdList;
    QMap<int, Task*> m_taskMap;
    QMap<int, DataUser> m_userMap;

    ActionMap m_actionMap;
    QByteArray m_sessionId;

private:
    DataUser m_user;
    GlobalNetworkObject *m_networkObject;
    TaskApp *m_app;
};


inline
QNetworkAccessManager* TaskGlobal::getNetworkManager()
{
    return m_instance->m_networkObject->getNetworkManager();
}

#endif // TASKGLOBAL_H
