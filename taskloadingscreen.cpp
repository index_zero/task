#include "taskloadingscreen.h"
#include "taskglobal.h"

TaskLoadingScreen::TaskLoadingScreen(GlobalListener *listener, QWidget *parent) :
    QLabel(parent),
    m_listener(listener)
{
    QSpacerItem *top = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QSpacerItem *bottom = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QSpacerItem *left = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QSpacerItem *right = new QSpacerItem(10, 10, QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    QHBoxLayout *gridh = new QHBoxLayout;
    QVBoxLayout *gridv = new QVBoxLayout;

    m_label = new TaskIconLabel;
    m_progressBar = new QProgressBar;
    m_cancelBtn = new QPushButton("Cancel");

    m_progressBar->setMinimum(0);

    QLabel *bg = new QLabel;

    QPalette p;
    QColor c = p.color(QPalette::Window);
    QString style;

    m_error = false;

    // initialize background and "dialog"
    bg->setObjectName("taskloadingwidgetbg");
    style.sprintf("#taskloadingwidgetbg {"
                  "border-radius: 10px;"
                  "border: 2px solid black;"
                  "padding-left: 10px;"
                  "padding-right: 10px;"
                  "margin: 10px;"
                  "background-color: %s; }",
                  c.name().toLocal8Bit().data());
    bg->setStyleSheet(style);
    bg->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    bg->setMinimumHeight(150);
    bg->setMinimumWidth(350);

    setObjectName("backgroudgradient");
    setStyleSheet(
        "#backgroudgradient { "
        "margin: 0px;"
        "background-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:1,"
            "stop:0 #404040,stop:1 #000);"
        "color: #cfcfcf; }");
    //left->setMinimumWidth(100);
    setMinimumWidth(100);

    // add to bg
    QGridLayout *grid = new QGridLayout;
    grid->addWidget(m_label, 0, 0, 1, 3);
    grid->addWidget(m_progressBar, 1, 0, 1, 3);
    grid->addWidget(m_cancelBtn, 2, 2);

    bg->setLayout(grid);

    // setup layout
    gridh->addItem(left);
    gridh->addWidget(bg, 0);
    gridh->addItem(right);

    gridv->addItem(top);
    gridv->addLayout(gridh, 0);
    gridv->addItem(bottom);

    connect(m_cancelBtn, SIGNAL(clicked()), this, SLOT(onCancel()));

    setLayout(gridv);

}

void TaskLoadingScreen::loadUserData(const int id)
{
    m_label->setLabel("Login Successful.  Loading data...", QIcon(":/images/16x16/network_folder.png"));

    TaskGlobal::loadUserData(m_listener, id);
//    TaskGlobal::PostDataMap d;

//    QByteArray b;
//    b.append(QString::number(id));
//    d.insert("id", b);

//    TaskGlobal::rawTaskQuery(m_listener, d, qUSERDATA);
}

void TaskLoadingScreen::beginState()
{
    m_cancelBtn->setText("Cancel");
    m_error = false;
    m_progressBar->setValue(0);
}

void TaskLoadingScreen::endState()
{

}

void TaskLoadingScreen::onCancel()
{
    if (m_error) {
        TaskGlobal::getApp()->MainWindow()->setTaskState(TaskMainWindow::TASK_STATE_LOGIN);
    } else {
 //TaskGlobal::getApp()->MainWindow()->cancelLoading();
    }
}

void TaskLoadingScreen::setLabel(const QString& sz, const QIcon& icon)
{
    m_label->setLabel(sz, icon);
}

void TaskLoadingScreen::initLoadData(const int taskcnt, const int usercnt)
{
    m_taskCnt = taskcnt;
    m_taskCur = 0;

    m_userCnt = usercnt;
    m_userCur = 0;

    m_progressBar->setMaximum(1 + taskcnt + usercnt);
}

void TaskLoadingScreen::displayError(const QString& err)
{
    setLabel(err, QIcon(":/images/16x16/cancel.png"));

    m_cancelBtn->setText("Ok");
    m_error = true;
    m_progressBar->setValue(m_progressBar->maximum());
}


void TaskLoadingScreen::updateProgress()
{
    m_progressBar->setValue(1 + m_userCur + m_taskCur);

    repaint();
}

void TaskLoadingScreen::loadedUser()
{
    if (m_userCur < m_userCnt) {
        m_userCur++;
    }

    updateProgress();
}

void TaskLoadingScreen::loadedTask()
{
    if (m_taskCur < m_taskCnt) {
        m_taskCur++;
    }

    updateProgress();
}
